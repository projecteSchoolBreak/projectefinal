﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    //Booleano que dice si el juego esta pausado
    public static bool GameIsPaused = false;
    //Objeto del menu de pausa
    public GameObject PauseMenuUI;

    public GameObject player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //En caso de pulsar escape, se pausara el juego o se reiniciara en funcion de su estado actual
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                resume();
            }
            else
            {
                pause();
            }
        }
    }
    //Funcion de reanudar el juego
    public void resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    //Funcion que pausa el juego
    void pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    //Funcion que te lleva a la pantalla principal
    public void mainmenu()
    {
        //player = GameObject.Find("Player");
        Destroy(player.transform.parent.gameObject);
        Time.timeScale = 1f;
        GameObject.Find("MainController").GetComponent<MainController>().PlayMenuMusic();
        SceneManager.LoadScene("TitleScreen");
    }

    //Funcion de salir del juego
    public void Exit()
    {
        Application.Quit();
    }
}
