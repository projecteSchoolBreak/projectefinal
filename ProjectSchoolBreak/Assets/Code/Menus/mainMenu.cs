﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    //Funcion que carga la escena de selector de personaje
    public void playGame()
    {
        SceneManager.LoadScene("Selector");
    }
    //Funcion que cierra la aplicacion
    public void Exit()
    {
        Application.Quit();
    }
}
