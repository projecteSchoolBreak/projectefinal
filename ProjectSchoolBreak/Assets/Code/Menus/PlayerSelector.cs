﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerSelector : MonoBehaviour
{
    public MainController main;

    //Elementos del menu de los personajes jugables
    public GameObject geekChar;
    public GameObject athleteChar;
    public GameObject cheerleaderChar;
    public GameObject fatChar;
    public GameObject asianChar;

    public void Start()
    {
        LoadGame();
    }
    //Funcion que devuelve al menu principal
    public void MainMenu()
    {
        SceneManager.LoadScene("TitleScreen");
    }
    //Funcion que crea al jugador en funcion del que le hayan enviado y carga la escena de juego
    public void StartGame(GameObject a)
    {
        main = GameObject.Find("MainController").GetComponent<MainController>();
        main.transform.position = new Vector3(0, 0, 10);
        GameObject b = Instantiate(a, new Vector3(0f, -2f, 0f), Quaternion.identity);
        main.PlayBattleMusic();
        SceneManager.LoadScene("Mapa");
    }
    //Esta funcion carga la partida guardada en caso de que haya, y desbloquea los personajes en funcion de los que haya en el archivo
    public void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/schoolBreak2.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/schoolBreak2.save", FileMode.Open);
            Save unblocks = (Save)bf.Deserialize(file);
            if (unblocks.unlockAtleta)
            {
                UnblockChar(1);
            }
            if (unblocks.unlockAnimadora)
            {
                UnblockChar(2);
            }
            if (unblocks.unlockGordo)
            {
                UnblockChar(3);
            }
            if (unblocks.unlockAsiatico)
            {
                UnblockChar(4);
            }
            file.Close();
            Debug.Log("Game Loaded");
        }
        else
        {
            //En caso de no haber archivo guardado, solo el primer personaje estara disponible
            Debug.Log("No game saved!");
        }
    }
    //Funcion que desbloquea el personaje que le envien
    //Al desbloquearlos, les devuelve el color y activa los botones
    public void UnblockChar(int pj)
    {
        switch (pj)
        {
            case 0:
                geekChar.GetComponent<Image>().color = Color.white;
                geekChar.GetComponent<Button>().enabled = true;
                break;
            case 1:
                athleteChar.GetComponent<Image>().color = Color.white;
                athleteChar.GetComponent<Button>().enabled = true;
                break;
            case 2:
                cheerleaderChar.GetComponent<Image>().color = Color.white;
                cheerleaderChar.GetComponent<Button>().enabled = true;
                break;
            case 3:
                fatChar.GetComponent<Image>().color = Color.white;
                fatChar.GetComponent<Button>().enabled = true;
                break;
            case 4:
                asianChar.GetComponent<Image>().color = Color.white;
                asianChar.GetComponent<Button>().enabled = true;
                break;
            default:
                break;
        }
    }
}
