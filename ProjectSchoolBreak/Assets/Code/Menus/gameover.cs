﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameover : MonoBehaviour
{
    //Funcion que nos llevara al menu principal
    public void mainmenu()
    {
        GameObject.Find("MainController").GetComponent<MainController>().PlayMenuMusic();
        SceneManager.LoadScene("TitleScreen");
    }

    //funcion que cerrara el juego por completo
    public void exitgame()
    {
        Application.Quit();
    }
}
