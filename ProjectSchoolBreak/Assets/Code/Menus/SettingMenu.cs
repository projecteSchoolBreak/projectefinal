﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SettingMenu : MonoBehaviour
{
    //mezclador de volumen general del juego
    public AudioMixer audioMix;
    //dropdown donde aparecerean las resoluciones
    public Dropdown resolutionDropdown;
    //lista de resoluciones
    Resolution[] resolutions;

    //buscamos todas las resoluciones de las que dispone nuestro dispositivo
    private void Start()
    {
        resolutions=Screen.resolutions;
        resolutionDropdown.ClearOptions();
        int currentRes = 0;
        List<string> options = new List<string>();
        for(int i=0; i<resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            //si la resolucion que esta listando es la que tenemos actualmente la asigna a una variable para despues cambiarla en el dropdown
            if (resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentRes = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentRes;
        resolutionDropdown.RefreshShownValue();
    }
    //Funcion que cambia la resolucion de pantalla
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
    //Funcion que cambia el volumen del juego
    public void SetVolume (float volume)
    {
        audioMix.SetFloat("volume", volume);
    }
    //Funcion que cambia la calidad grafica del juego
    public void SetQuality (int quality)
    {
        QualitySettings.SetQualityLevel(quality);
    }
    //Funcion que cambia la pantalla completa
    public void SetFullScreen (bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }
}
