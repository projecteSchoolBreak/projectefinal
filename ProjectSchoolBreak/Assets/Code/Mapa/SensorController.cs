﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorController : MonoBehaviour
{
    //Habitacion del sensor
    public RoomController room;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        //Si colisiona con el jugador activa la habitacion
        if(collision.gameObject.tag == "Player")
        {

            if (!room.activated)
            {

                room.player = collision.transform;
                room.ActivateRoom();

            }

        }

    }

}
