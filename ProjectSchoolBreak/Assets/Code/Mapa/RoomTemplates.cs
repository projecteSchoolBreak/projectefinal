﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTemplates : MonoBehaviour {

    //Lista de prefabs de las habitaciones que se pueden generar
	public List<GameObject> bottomRooms;
	public List<GameObject> topRooms;
	public List<GameObject> leftRooms;
	public List<GameObject> rightRooms;
    //Prefab de habitacion cerrada en caso de que se sobrepongan habitaciones
	public GameObject closedRoom;
    //Lista de habitaciones creadas
	public List<GameObject> rooms;
    //Tiempo de espera antes de crear la tienda y la habitacion del tesoro
	public float waitTime;
    //Booleano que indica si se ha creado el boss
	private bool spawnedBoss;
    //Booleano que indica si se ha creado la tienda
    private bool spawnedShop;
    //Booleano que indica si se ha creado el tesoro
    private bool spawnedTreasure;
    //Lista de bosses que se pueden crear
    public List<GameObject> bossPool;
    //Lista de habitaciones tienda
    public List<GameObject> shopRooms;
    //Lista de habitaciones del tesoro
    public List<GameObject> treasureRooms;
    //Ventana de carga
    public GameObject loading;
    //Interficie del jugador
    public GameObject canvas;

    private void Start()
    {
        //Obtenemos la ventana de carga y la interficie del jugador
        loading = GameObject.Find("Player").GetComponent<playerController>().loadingScreen;
        canvas = GameObject.Find("Player").GetComponent<playerController>().canvas;
        loading.SetActive(true);
        canvas.SetActive(false);
    }

    void Update(){
		if(waitTime <= 0 && spawnedBoss == false){
            //Una vez haya pasado el tiempo de espera se escanea el mapa para preparar el astarpath y el pathfinding
            AstarPath.active.Scan();
            //Numero de habitacion en la que se creara el boss
            int habitacion = rooms.Count - 1;
            Debug.Log("Paso el tiempo");
            for (int i = rooms.Count-1; i > 0; i--)
            {
                //Recorremos la lista de habitaciones buscando una con una sola entrada
                if (rooms[i].GetComponent<RoomController>().oneEntrance)
                {
                    habitacion = i;
                    if (!spawnedBoss)
                    {
                        //Si no se ha creado un boss ya, lo creamos en esta sala
                        rooms[habitacion].GetComponent<RoomController>().bossIsHere = true;
                        spawnedBoss = true;
                    }
                    else if (!spawnedShop)
                    {
                        //Si no se ha creado una tienda ya, la creamos en esta sala
                        if (rooms[habitacion].name == "B(Clone)" || rooms[habitacion].name == "B2(Clone)")
                        {
                            Instantiate(shopRooms[0], rooms[habitacion].transform.position, shopRooms[0].transform.rotation);
                            GameObject a = rooms[habitacion];
                            rooms.Remove(a);
                            Destroy(a);
                        }
                        else if (rooms[habitacion].name == "T(Clone)" || rooms[habitacion].name == "T2(Clone)")
                        {
                            Instantiate(shopRooms[1], rooms[habitacion].transform.position, shopRooms[1].transform.rotation);
                            GameObject a = rooms[habitacion];
                            rooms.Remove(a);
                            Destroy(a);
                        }
                        else if (rooms[habitacion].name == "L(Clone)" || rooms[habitacion].name == "L2(Clone)")
                        {
                            Instantiate(shopRooms[2], rooms[habitacion].transform.position, shopRooms[2].transform.rotation);
                            GameObject a = rooms[habitacion];
                            rooms.Remove(a);
                            Destroy(a);
                        }
                        else if (rooms[habitacion].name == "R(Clone)" || rooms[habitacion].name == "R2(Clone)")
                        {
                            Instantiate(shopRooms[3], rooms[habitacion].transform.position, shopRooms[3].transform.rotation);
                            GameObject a = rooms[habitacion];
                            rooms.Remove(a);
                            Destroy(a);
                        }
                        spawnedShop = true;
                    }
                    else if (!spawnedTreasure)
                    {
                        //Si no se ha creado una habitacion del tesoro ya, la creamos en esta sala
                        if (rooms[habitacion].name == "B(Clone)" || rooms[habitacion].name == "B2(Clone)")
                        {
                            Instantiate(treasureRooms[0], rooms[habitacion].transform.position, treasureRooms[0].transform.rotation);
                            GameObject a = rooms[habitacion];
                            rooms.Remove(a);
                            Destroy(a);
                        }
                        else if (rooms[habitacion].name == "T(Clone)" || rooms[habitacion].name == "T2(Clone)")
                        {
                            Instantiate(treasureRooms[1], rooms[habitacion].transform.position, treasureRooms[1].transform.rotation);
                            GameObject a = rooms[habitacion];
                            rooms.Remove(a);
                            Destroy(a);
                        }
                        else if (rooms[habitacion].name == "L(Clone)" || rooms[habitacion].name == "L2(Clone)")
                        {
                            Instantiate(treasureRooms[2], rooms[habitacion].transform.position, treasureRooms[2].transform.rotation);
                            GameObject a = rooms[habitacion];
                            rooms.Remove(a);
                            Destroy(a);
                        }
                        else if (rooms[habitacion].name == "R(Clone)" || rooms[habitacion].name == "R2(Clone)")
                        {
                            Instantiate(treasureRooms[3], rooms[habitacion].transform.position, treasureRooms[3].transform.rotation);
                            GameObject a = rooms[habitacion];
                            rooms.Remove(a);
                            Destroy(a);
                        }
                        spawnedTreasure = true;
                    }
                }
            }
            loading.SetActive(false);
            canvas.SetActive(true);
        } else {
			waitTime -= Time.deltaTime;
		}
	}
}
