﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddRoom : MonoBehaviour {
    //Objeto que controla la generacion de salas
	private RoomTemplates templates;

	void Start(){
        //Anade esta habitacion a la lista del manager
		templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
		templates.rooms.Add(this.gameObject);
	}
}
