﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSpawner : MonoBehaviour {

    //Direccion de salida del generador
	public int openingDirection;
    // 1 --> necesita salida inferior
    // 2 --> necesita salida superior
    // 3 --> necesita salida izquierda
    // 4 --> necesita salida derecha

    //Objeto que controla la creacion de salas
    private RoomTemplates templates;
    //Numero aleatorio
	private int rand;
    //Booleano que indica si ya se ha generado una nueva sala
	public bool spawned = false;
    //Tiempo que espera el objeto antes de destruirse
	public float waitTime = 4f;

	void Start(){
		Destroy(gameObject, waitTime);
		templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        //Invocamos la funcion de generar sala
		Invoke("Spawn", 0.2f);
	}
    //Funcion que genera una sala en la direccion de salida
	void Spawn(){
		if(spawned == false){
			if(openingDirection == 1){
				//Genera una sala con una salida inferior
				rand = Random.Range(0, templates.bottomRooms.Count);
				Instantiate(templates.bottomRooms[rand], transform.position, templates.bottomRooms[rand].transform.rotation);
			} else if(openingDirection == 2){
                //Genera una sala con una salida superior
                rand = Random.Range(0, templates.topRooms.Count);
				Instantiate(templates.topRooms[rand], transform.position, templates.topRooms[rand].transform.rotation);
			} else if(openingDirection == 3){
                //Genera una sala con una salida izquierda
                rand = Random.Range(0, templates.leftRooms.Count);
				Instantiate(templates.leftRooms[rand], transform.position, templates.leftRooms[rand].transform.rotation);
			} else if(openingDirection == 4){
                //Genera una sala con una salida derecha
                rand = Random.Range(0, templates.rightRooms.Count);
				Instantiate(templates.rightRooms[rand], transform.position, templates.rightRooms[rand].transform.rotation);
			}
			spawned = true;
		}
	}
    //Si colisiona con otro generador, lo destruye y marca este como activado
	void OnTriggerEnter2D(Collider2D other){
		if(other.CompareTag("SpawnPoint")){
			if(other.GetComponent<RoomSpawner>().spawned == false && spawned == false){
				Instantiate(templates.closedRoom, transform.position, Quaternion.identity);
				Destroy(gameObject);
			} 
			spawned = true;
		}
	}
}
