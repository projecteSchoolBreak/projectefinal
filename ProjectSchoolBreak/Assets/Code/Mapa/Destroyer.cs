﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {

    //Tiempo de espera antes de destruirse
    public float waitTime = 4f;

    void Start()
    {
        Destroy(gameObject, waitTime);
    }
    //Destruye todo con lo que colisiona
    void OnTriggerEnter2D(Collider2D other){
        
		Destroy(other.gameObject);
		
	}

}
