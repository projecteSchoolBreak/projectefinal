﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class RoomController : MonoBehaviour
{
    //Booleano que indica si se ha activado la habitacion
    public bool activated = false;
    //Booleano que indica si se ha generado un drop
    public bool hasDropped = false;
    //Muros temporales que bloquean las salidas de la sala
    public GameObject[] tempWalls;
    //Sensores que detectan si ha entrado el jugador a la sala
    public GameObject[] sensors;
    //Transform del jugador
    public Transform player;
    //Transform del punto de spawn del boss
    public Transform bossSpawnPoint;
    //Probabilidad de que aparezca un drop al completar la habitacion
    public int dropChances = 80;
    //Lista de drops que se pueden generar
    public GameObject[] posibleDrops;
    //Booleano que indica si el boss esta en esta habitacion
    public bool bossIsHere = false;
    //Booleano que indica si esta habitacion solo tiene un entrada
    public bool oneEntrance;
    //Objeto prefab de las escaleras al siguiente nivel
    public GameObject stairs;
    //Posicion del drop de la sala
    public Vector3 normalDropPlace = new Vector3(0f,0f,0f);
    //Posicion de las escaleras
    public Vector3 stairDropPlace = new Vector3(0f, 2f, 0f);
    
    [Header("Enemies")]
    //Lista de posibles bosses
    public List<GameObject> bosses;
    //Lista de Dummy bosses
    public List<GameObject> falseBosses;
    //Objeto boss que se ha creado
    public GameObject boss;
    //Objeto Dummy boss que se ha creado
    public GameObject falseBoss;
    //Lista de posibles enemigos
    public List<GameObject> enemies;
    //Numero de enemigos a generar
    public int numberOfEnemies;
    //Contenedor de enemigos
    public GameObject enemyContainer;
    //Posicion de generacion de los enemigos
    public List<Vector3> enemySpawns;
    //Lista de enemigos en la habitacion
    private List<GameObject> enemiesInTheRoom;
    //Booleano que indica si el boss creado es el profesor de gimnasia
    bool esGimnasia = false;
    //Booleano que indica si se ha respondido al quiz
    bool respondido = false;

    // Start is called before the first frame update
    void Start()
    {
        //Creamos una nueva lista de enemigos vacia
        enemiesInTheRoom = new List<GameObject>();
        Debug.Log("He nacido");

    }

    // Update is called once per frame
    void Update()
    {
        
        if(enemyContainer.transform.childCount == 0 && activated)
        {
            //Si se han derrotado a todos los enemigos de la sala, se desbloquearan las salidas y se llamara a la funcion del drop
            OpenRoom();
            if (!hasDropped)
            {

                hasDropped = true;
                Drops();

            }

        }

    }

    private void Drops()
    {
        //Generamos un numero aleatorio, y si es menor que las probabilidades de dropeo, se generara un objeto aleatorio de la lista de drops
        int a = Random.Range(0, 100);
        if(a < dropChances)
        {

            int b = Random.Range(0, posibleDrops.Length);

            GameObject dropped = Instantiate(posibleDrops[b]);
            dropped.transform.parent = this.transform;
            dropped.transform.localPosition = normalDropPlace;

        }
        //En el caso de que hubiera un boss en la habitacion, se generan tambien las escaleras al siguiente piso
        if (bossIsHere)
        {

            GameObject nextFloor = Instantiate(stairs);
            nextFloor.transform.parent = this.transform;
            nextFloor.transform.localPosition = stairDropPlace;

        }

    }
    //Funcion que abre las puertas de la habitacion
    void OpenRoom()
    {

        foreach (GameObject wall in tempWalls)
        {

            wall.SetActive(false);

        }

    }
    //Funcion que se llama cuando se detecta la entrada del jugador en la sala
    public void ActivateRoom()
    {
        //Se activan los muros para bloquear las entradas
        foreach (GameObject wall in tempWalls)
        {

            wall.SetActive(true);

        }

        //Si hay un boss en esta habitacion, se generara uno aleatoriamente, y se dejara desactivado hasta que se responda la pregunta del Dummy Boss
        if (bossIsHere)
        {

            int a = Random.Range(0, bosses.Count);

            boss = Instantiate(bosses[a], bossSpawnPoint.position, Quaternion.identity);
            falseBoss = Instantiate(falseBosses[a], bossSpawnPoint.position, Quaternion.identity);

            if (a == 0)
            {
                esGimnasia = true;
                boss.transform.GetChild(0).GetChild(0).GetComponentInChildren<Gimnasia>().player = player;
                boss.transform.GetChild(0).GetComponent<enemy>().maxHp = boss.transform.GetChild(0).GetComponent<enemy>().maxHp * MainController.nivel;
                boss.transform.GetChild(0).GetComponent<enemy>().currHp = boss.transform.GetChild(0).GetComponent<enemy>().currHp * MainController.nivel;
            }
            else
            {
                boss.GetComponent<enemy>().maxHp = boss.GetComponent<enemy>().maxHp * MainController.nivel;
                boss.GetComponent<enemy>().currHp = boss.GetComponent<enemy>().currHp * MainController.nivel;
            }
            boss.transform.SetParent(enemyContainer.transform);
            falseBoss.transform.SetParent(this.transform);
            falseBoss.GetComponent<DummyBoss>().myRoom = this;
            boss.SetActive(false);
            
            enemiesInTheRoom.Add(boss);

        }
        else
        {
            //Si no hay un boss en la sala, se genera el numero de enemigos especificado
            for (int i = 0; i < numberOfEnemies; i++)
            {

                GameObject a = Instantiate(enemies[Random.Range(0, enemies.Count)], enemyContainer.transform);
                a.transform.localPosition = enemySpawns[i];
                a.transform.rotation = Quaternion.identity;
                a.GetComponent<enemy>().maxHp = a.GetComponent<enemy>().maxHp + (5*(MainController.nivel - 1));
                a.GetComponent<enemy>().currHp = a.GetComponent<enemy>().maxHp;
                enemiesInTheRoom.Add(a);

            }

            foreach (GameObject enem in enemiesInTheRoom)
            {

                enem.GetComponent<AIDestinationSetter>().target = player;

            }

        }

        activated = true;

    }

    //Una vez respondida la pregunta del Dummy Boss, si se ha respondido incorrectamente se doblara la vida del boss
    public void BuffarBoss(bool buff)
    {

        if (buff && !respondido)
        {

            if (esGimnasia)
            {

                boss.transform.GetChild(0).GetComponent<enemy>().maxHp = boss.transform.GetChild(0).GetComponent<enemy>().maxHp * 2;
                boss.transform.GetChild(0).GetComponent<enemy>().currHp = boss.transform.GetChild(0).GetComponent<enemy>().currHp * 2;

            }
            else
            {

                boss.GetComponent<enemy>().maxHp = boss.GetComponent<enemy>().maxHp * 2;
                boss.GetComponent<enemy>().currHp = boss.GetComponent<enemy>().currHp * 2;

            }
            respondido = true;

        }

        boss.SetActive(true);
        falseBoss.SetActive(false);

    }

}
