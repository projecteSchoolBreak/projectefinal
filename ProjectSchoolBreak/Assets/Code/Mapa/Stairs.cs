﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stairs : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.gameObject.tag == "Player")
        {
            //Al colisionar con el jugador, aumenta el nivel en el que esta y vuelve a cargar la escena de juego
            MainController.masNivel();
            this.gameObject.SetActive(false);
            collision.transform.position = new Vector3(1, -2, 0);
            SceneManager.LoadScene("Mapa");

        }

    }

}
