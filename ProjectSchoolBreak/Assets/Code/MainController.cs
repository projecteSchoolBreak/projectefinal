﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    //Objeto main usado para hacer el singleton
    static GameObject main = null;
    //Lista de objetos disponibles para la tienda y cofres
    public List<GameObject> itemPool;
    //Cartel de descripcion de los objetos
    public GameObject canvas;
    //Nombre del objeto obtenido
    public Text itemName;
    //Descripcion del objeto obtenido
    public Text itemDesc;
    //Nivel actual
    public static int nivel = 1;
    //Pregunta actual
    public Text pregunta;
    //Lista de respuestas posibles
    public List<Text> respuestas;
    //Lista de botones de las respuestas
    public List<GameObject> botones;
    //Fondo de la pregunta
    public GameObject fondoPregunta;
    //DummyBoss que controla las preguntas
    public DummyBoss dummy;
    //Camara del juego
    public Camera gameCamera;
    //Sonido de respuesta incorrecta
    public AudioClip badAnswer;
    //Sonido de respuesta correcta
    public AudioClip goodAnswer;
    //Manager de sonido
    public AudioSource audioSource;
    //Manage de banda sonora
    public AudioSource musicaFondo;
    //Musica de menu
    public AudioClip menuMusic;
    //Musica de combate
    public AudioClip battleMusic;


    void Awake()
    {
        //Patró singleton
        if (main == null)
        {
            //crea el objecte en el primer moment
            main = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena.
            //D'aquesta manera no es destrueix al carregarse
            DontDestroyOnLoad(main);
        }
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }
    }
    //Funcion llamada al recoger un objeto
    public void GetItem(Item objeto)
    {
        //Se escriben los datos del objeto en el canvas y se muestra el mensaje por pantalla
        itemName.text = objeto.itemName;
        itemDesc.text = objeto.itemDescription;
        canvas.GetComponent<Animator>().Play("ItemPopUp");
        //Tras mostrarse se llama a la funcion para esconder el mensaje
        StartCoroutine(HidePopUp());

    }
    //Funcion de respuesta del quiz
    public void Respuesta(int a)
    {

        //Se envia la respuesta del jugador al Dummy boss y se comprueba si es correcta
        dummy.playerAnswer = a;
        dummy.Comprobar();

    }
    //Aumenta el nivel del juego
    public static void masNivel()
    {

        nivel++;

    }
    //Funcion de desbloqueo de personajes
    public static void UnlockNewChar()
    {
        
        if (File.Exists(Application.persistentDataPath + "/schoolBreak2.save"))
        {
            //Si ya existe un fichero de guardado, se abrira el fichero y se comprobaran los personajes desbloqueados
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file3 = File.Open(Application.persistentDataPath + "/schoolBreak2.save", FileMode.Open);
            Save unblocks = (Save)bf.Deserialize(file3);
            file3.Close();
            //Al hacer la comprobacion, se desbloqueara el primer personaje bloqueado que encuentre
            if (unblocks.unlockAtleta)
            {
                if (unblocks.unlockAnimadora)
                {
                    if (unblocks.unlockGordo)
                    {
                        if (unblocks.unlockAsiatico)
                        {
                            
                        }
                        else
                        {
                            //crees el objecte save
                            Save unlockables = CreateSave(true, true, true, true);
                            //crees un BinaryFormatter que es com un OOS
                            BinaryFormatter bf2 = new BinaryFormatter();
                            //Crees el File
                            File.Delete(Application.persistentDataPath + "/schoolBreak2.save");
                            FileStream file2 = File.Create(Application.persistentDataPath + "/schoolBreak2.save");
                            //serialitzes
                            bf2.Serialize(file2, unlockables);
                            file2.Close();
                            Debug.Log("Game Saved");
                        }
                    }
                    else
                    {
                        //crees el objecte save
                        Save unlockables = CreateSave(true, true, true, false);
                        //crees un BinaryFormatter que es com un OOS
                        BinaryFormatter bf2 = new BinaryFormatter();
                        //Crees el File
                        File.Delete(Application.persistentDataPath + "/schoolBreak2.save");
                        FileStream file2 = File.Create(Application.persistentDataPath + "/schoolBreak2.save");
                        //serialitzes
                        bf2.Serialize(file2, unlockables);
                        file2.Close();
                        Debug.Log("Game Saved");
                    }
                }
                else
                {
                    //crees el objecte save
                    Save unlockables = CreateSave(true, true, false, false);
                    //crees un BinaryFormatter que es com un OOS
                    BinaryFormatter bf2 = new BinaryFormatter();
                    //Crees el File
                    File.Delete(Application.persistentDataPath + "/schoolBreak2.save");
                    FileStream file2 = File.Create(Application.persistentDataPath + "/schoolBreak2.save");
                    //serialitzes
                    bf2.Serialize(file2, unlockables);
                    file2.Close();
                    Debug.Log("Game Saved");
                }
            }
            else
            {
                //crees el objecte save
                Save unlockables = CreateSave(true, false, false, false);
                //crees un BinaryFormatter que es com un OOS
                BinaryFormatter bf2 = new BinaryFormatter();
                //Crees el File
                File.Delete(Application.persistentDataPath + "/schoolBreak2.save");
                FileStream file2 = File.Create(Application.persistentDataPath + "/schoolBreak2.save");
                //serialitzes
                bf2.Serialize(file2, unlockables);
                file2.Close();
                Debug.Log("Game Saved");
            }
            Debug.Log("Game Loaded");

        }
        else
        {
            //Si no existe un fichero de guardado, crea uno nuevo y desbloquea al primer personaje
            Debug.Log("No game saved!");
            //crees el objecte save
            Save unlockables = CreateSave(true, false, false, false);
            //crees un BinaryFormatter que es com un OOS
            BinaryFormatter bf2 = new BinaryFormatter();
            //Crees el File
            FileStream file2 = File.Create(Application.persistentDataPath + "/schoolBreak2.save");
            //serialitzes
            bf2.Serialize(file2, unlockables);
            file2.Close();
            Debug.Log("Game Saved");
        }

    }
    //Funcion que crea un nuevo archivo de guardado segun los booleanos que le envien
    public static Save CreateSave(bool atleta, bool animadora, bool gordo, bool chino)
    {

        Save unlockables = new Save();

        unlockables.unlockAtleta = atleta;
        unlockables.unlockAnimadora = animadora;
        unlockables.unlockGordo = gordo;
        unlockables.unlockAsiatico = chino;

        return unlockables;

    }
    //Funcion que reproduce el sonido de respuesta correcta
    public void PlayCorrectAnswer()
    {

        audioSource.clip = goodAnswer;
        audioSource.Play();

    }
    //Funcion que reproduce el sonido de respuesta incorrecta
    public void PlayBadAnswer()
    {

        audioSource.clip = badAnswer;
        audioSource.Play();

    }
    public void PlayMenuMusic()
    {
        musicaFondo.clip = menuMusic;
        musicaFondo.Play();
    }
    public void PlayBattleMusic()
    {
        musicaFondo.clip = battleMusic;
        musicaFondo.Play();
    }
    //Corutina que esconde el mensaje de descripcion de los objetos
    IEnumerator HidePopUp()
    {

        yield return new WaitForSeconds(2f);
        canvas.GetComponent<Animator>().Play("ItemClosePopUp");

    }

}
