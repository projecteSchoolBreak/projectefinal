﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProyectil : MonoBehaviour
{
    //Dano del proyectil
    public int damage;
    //Booleano que indica si el proyectil ralentiza
    public bool slows = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {

        //Si colisiona con el jugador o con un substituto lo dañara
        if (collision.gameObject.tag == "Player") {

            collision.gameObject.GetComponent<playerController>().dolor(damage);
            if (slows)
            {
                collision.gameObject.GetComponent<playerController>().slow();
            }

        }
        if (collision.gameObject.tag == "Substitute") collision.gameObject.GetComponent<AlmohadaController>().dolor(damage);
        //En el caso de colisionar con otro proyectil enemigo o con un enemigo, no se destruira este proyectil
        if(collision.gameObject.tag != "EnemyProyectil" && collision.gameObject.tag != "Enemy")
        {
            Destroy(gameObject);
        }

    }
}
