﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    public int maxHp = 5;
    public int currHp;
    public List<GameObject> charco;
    public bool imBoss = false;
    public GameObject coin;
    [HideInInspector]
    public float originalSpeed;
    public bool charm;
    public SpriteRenderer sprites;

    // Start is called before the first frame update
    void Start()
    {
        currHp = maxHp;

        originalSpeed = this.gameObject.GetComponent<AILerp>().speed;

        sprites = GetComponent<SpriteRenderer>();
    }

    public void damageado(int damage)
    {

        currHp -= damage;
        sprites.color = Color.red;
        StartCoroutine(vuelvecolor(0.2f));

        if (currHp<=0)
        {
            morite();
        }

    }

    IEnumerator vuelvecolor(float time)
    {
        yield return new WaitForSeconds(time);
        sprites.color = Color.white;
    }

    public void morite()
    {

        GameObject a = Instantiate(charco[Random.Range(0, charco.Count)]);
        a.transform.position = this.transform.position;
        if (imBoss)
        {
            a.transform.localScale = new Vector3(a.transform.localScale.x * 2, a.transform.localScale.y * 2, a.transform.localScale.z * 2);
            Destroy(this.transform.parent.gameObject);
        }
        else
        {

            int rand = Random.Range(0, 100);
            if(rand < 70)
            {

                Instantiate(coin, this.transform.position, coin.transform.rotation);

            }
            Destroy(this.gameObject);

        }
        

    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "SuperRoll")
        {

            damageado(3);

        }

    }

    public void charmed()
    {

        this.gameObject.GetComponent<AILerp>().speed = this.gameObject.GetComponent<AILerp>().speed * 0.5f;
        this.gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 0, 190);
        charm = true;
        StartCoroutine(Recover(2f));

    }

    public void slowed()
    {

        this.gameObject.GetComponent<AILerp>().speed = this.gameObject.GetComponent<AILerp>().speed * 0.5f;
        StartCoroutine(Recover(2f));

    }

    public void poisoned(int damage)
    {

        StartCoroutine(Poison(1f, damage));

    }

    public void burned(int damage)
    {

        StartCoroutine(Burn(1f, damage));

    }

    public void paralyzed()
    {

        this.gameObject.GetComponent<AILerp>().canMove = false;
        StartCoroutine(Paralyze(1f));

    }

    IEnumerator Poison(float time, int damage)
    {

        yield return new WaitForSeconds(time);
        damageado(damage);
        yield return new WaitForSeconds(time);
        damageado(damage);
        yield return new WaitForSeconds(time);
        damageado(damage);

    }

    IEnumerator Burn(float time, int damage)
    {

        yield return new WaitForSeconds(time);
        damageado(damage);
        yield return new WaitForSeconds(time);
        damageado(damage);
        yield return new WaitForSeconds(time);
        damageado(damage);

    }

    IEnumerator Paralyze(float time)
    {

        yield return new WaitForSeconds(time);
        this.gameObject.GetComponent<AILerp>().canMove = true;

    }

    IEnumerator Recover(float time)
    {

        yield return new WaitForSeconds(time);
        this.gameObject.GetComponent<AILerp>().speed = originalSpeed;
        this.gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
        charm = false;

    }

}
