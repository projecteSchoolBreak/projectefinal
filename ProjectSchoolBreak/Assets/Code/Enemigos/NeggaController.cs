﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeggaController : MonoBehaviour
{

    //Objeto prefab de los proyectiles enemigos
    public GameObject proyectil;
    //Tiempo de enfriamiento de los ataques
    public int cd;
    //Booleano que indica si se puede atacar
    public bool available;
    //Fuerza con la que se lanzan los proyectiles
    public float fuerza;
    //Dano de los proyectiles
    public int damage;
    //Cantidad de desvio en los disparos
    public float desvio;
    //Numero de disparos del enemigo
    public int nShots = 3;
    //Objeto enemigo
    public GameObject yo;

    // Start is called before the first frame update
    void Start()
    {
        available = true;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //En caso de que el jugador entre dentro del area de ataque, el enemigo se quedara inmovil y empezara a dispararle
        if (col.gameObject.transform == yo.GetComponent<AIDestinationSetter>().target)
        {
            if (!yo.GetComponent<enemy>().charm)
            {
                if (available)
                {
                    yo.GetComponent<AILerp>().canMove = false;
                    Vector2 dir = col.transform.position - this.transform.position;
                    for (int i = 0; i < nShots; i++)
                    {
                        disparo(dir);
                    }
                    available = false;
                    StartCoroutine(Recargar());
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        //En caso de que el jugador permanezca dentro del area de ataque, el enemigo seguira disparandole
        if (col.gameObject.transform == yo.GetComponent<AIDestinationSetter>().target)
        {
            if (!yo.GetComponent<enemy>().charm)
            {
                if (available)
                {
                    yo.GetComponent<AILerp>().canMove = false;
                    Vector2 dir = col.transform.position - this.transform.position;
                    for (int i = 0; i < nShots; i++)
                    {
                        disparo(dir);
                    }
                    available = false;
                    StartCoroutine(Recargar());
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //En caso de que el jugador salga del area de ataque, el enemigo recuperara la movilidad
        if (collision.gameObject.transform == yo.GetComponent<AIDestinationSetter>().target)
        {
            yo.GetComponent<AILerp>().canMove = true;
        }
    }
    //Funcion del disparo
    void disparo(Vector2 dir)
    {
        //Se añade el desvio de forma aleatoria al vector de direccion de los proyectiles
        dir = new Vector2(dir.x + Random.Range(-desvio, desvio), dir.y + Random.Range(-desvio, desvio));
        GameObject bala = Instantiate(proyectil, new Vector2(this.transform.position.x + (dir.x*0.2f), this.transform.position.y + (dir.y * 0.2f)), this.transform.rotation);
        bala.GetComponent<EnemyProyectil>().damage = damage;
        Rigidbody2D rb = bala.GetComponent<Rigidbody2D>();
        rb.AddForce(dir * fuerza, ForceMode2D.Impulse);
    }
    //Corutina que recupera el ataque al enemigo
    IEnumerator Recargar()
    {
        yield return new WaitForSeconds(cd);
        available = true;
    }

}
