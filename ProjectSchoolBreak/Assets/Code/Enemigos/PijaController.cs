﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PijaController : MonoBehaviour
{
    //Objeto prefab del flash de la camara
    public GameObject flash;
    //Tiempo de enfriamiento del ataque
    public int cd;
    //Booleano que indica la disponibilidad del ataque
    public bool available;
    //Objeto enemigo
    public GameObject yo;

    // Start is called before the first frame update
    void Start()
    {
        available = true;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //En caso de que el jugador entre dentro del area de ataque, el enemigo se quedara inmovil y empezara a dispararle
        if (col.gameObject.transform == yo.GetComponent<AIDestinationSetter>().target)
        {
            if (!yo.GetComponent<enemy>().charm)
            {
                if (available)
                {
                    yo.GetComponent<AILerp>().canMove = false;
                    GameObject a = Instantiate(flash);
                    Vector2 dir = col.transform.position - this.transform.position;
                    a.transform.position = new Vector2(this.transform.position.x + (dir.x * 0.6f), this.transform.position.y + (dir.y * 0.6f));
                    float angulo = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 330f;
                    a.GetComponent<Rigidbody2D>().rotation = angulo;
                    available = false;
                    StartCoroutine(Selfie());
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        //En caso de que el jugador permanezca dentro del area de ataque, el enemigo seguira disparandole
        if (col.gameObject.transform == yo.GetComponent<AIDestinationSetter>().target)
        {
            if (!yo.GetComponent<enemy>().charm)
            {
                if (available)
                {
                    yo.GetComponent<AILerp>().canMove = false;
                    GameObject a = Instantiate(flash);
                    Vector2 dir = col.transform.position - this.transform.position;
                    a.transform.position = new Vector2(this.transform.position.x + (dir.x * 0.6f), this.transform.position.y + (dir.y * 0.6f));
                    float angulo = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 330f;
                    a.GetComponent<Rigidbody2D>().rotation = angulo;
                    available = false;
                    StartCoroutine(Selfie());
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //En caso de que el jugador salga del area de ataque, el enemigo recuperara la movilidad
        if (collision.gameObject.transform == yo.GetComponent<AIDestinationSetter>().target)
        {
            yo.GetComponent<AILerp>().canMove = true;
        }
    }
    //Corutina que recupera el ataque al enemigo
    IEnumerator Selfie()
    {
        yield return new WaitForSeconds(cd);
        available = true;
    }

}
