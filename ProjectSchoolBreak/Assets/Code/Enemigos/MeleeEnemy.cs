﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : MonoBehaviour
{
    //Tiempo entre ataques del enemigo
    public float cooldown = 1f;
    //Booleano que indica si se esta recargando el ataque
    public bool recharging;
    //El objeto del enemigo
    public GameObject yo;

    void OnTriggerEnter2D(Collider2D col)
    {
        //En caso de que el jugador entre dentro del rango de accion y el enemigo pueda atacar, le danara
        if (col.gameObject.transform == yo.GetComponent<AIDestinationSetter>().target)
        {
            if (!recharging)
            {
                if (!yo.GetComponent<enemy>().charm)
                {
                    if (col.gameObject.tag == "Player") col.gameObject.GetComponent<playerController>().dolor(1);
                    if (col.gameObject.tag == "Substitute") col.gameObject.GetComponent<AlmohadaController>().dolor(1);
                }
                recharging = true;
                StartCoroutine(Recargar());
            }
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        //En caso de que el jugador permanezca dentro del rango de accion y el enemigo pueda atacar, le danara
        if (col.gameObject.tag == "Player")
        {
            if (!recharging)
            {
                if (!yo.GetComponent<enemy>().charm)
                {
                    if (col.gameObject.tag == "Player") col.gameObject.GetComponent<playerController>().dolor(1);
                    if (col.gameObject.tag == "Substitute") col.gameObject.GetComponent<AlmohadaController>().dolor(1);
                }
                recharging = true;
                StartCoroutine(Recargar());
            }
        }
    }
    //Corutina que devuelve la accion de ataque al enemigo tras el tiempo de enfriamiento
    IEnumerator Recargar()
    {
        yield return new WaitForSeconds(cooldown);
        recharging = false;
    }
}
