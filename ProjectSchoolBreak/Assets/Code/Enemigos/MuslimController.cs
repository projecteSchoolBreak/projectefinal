﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuslimController : MonoBehaviour
{
    //Animador del enemigo
    public Animator anim;
    //Booleano que indica si el jugador esta en rengo
    public bool inRange;
    //Objeto enemigo
    public GameObject yo;

    void OnTriggerEnter2D(Collider2D col)
    {
        //Si el jugador entra en el rango del enemigo, este se parara y empezara la autodestruccion
        if (col.gameObject.transform == yo.GetComponent<AIDestinationSetter>().target)
        {
            inRange = true;
            if (!yo.GetComponent<enemy>().charm)
            {
                Debug.Log("no charm");
                yo.GetComponent<AILerp>().speed = 0;
                yo.GetComponent<AILerp>().canMove = false;
                StartCoroutine(Explode(col.gameObject, 1.5f));
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //Si el jugador sale del rango de efecto, se desactivara el booleano
        if (collision.gameObject.tag == "Player")
        {
            inRange = false;
        }
    }
    //Corutina de explosion, tras un tiempo, hara una animacion y explotara, dañando al jugador si este esta en rango
    IEnumerator Explode(GameObject player, float time)
    {
        yield return new WaitForSeconds(time);
        anim.SetTrigger("kbom");
        if (inRange)
        {
            if (player.tag == "Player") player.gameObject.GetComponent<playerController>().dolor(2);
            if (player.tag == "Substitute") player.gameObject.GetComponent<AlmohadaController>().dolor(2);
        }

        yield return new WaitForSeconds(0.5f);
        yo.GetComponent<enemy>().morite();
    }

}
