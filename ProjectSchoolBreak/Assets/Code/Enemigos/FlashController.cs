﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Eliminaremos el objeto flash automaticamente
        Invoke("die", 0.1f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            //Si colisiona con el jugador lo confundira
            col.gameObject.GetComponent<playerController>().confusion();
        }
    }

    void die()
    {
        Destroy(this.gameObject);
    }

}
