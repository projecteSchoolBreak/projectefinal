﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSpot : MonoBehaviour
{
    //Booleano que indica si se ha comprado el producto
    public bool bought = false;
    //Precio del producto
    public int price = 2;
    //Objeto que se vende
    public GameObject product;
    //Texto del precio
    public Text texto;
    //Imagen del producto
    public GameObject cube;
    //Booleano que indica si el producto es un objeto o un drop
    public bool objeto = true;
    //GameObject a borrar de la pool de items del main
    public GameObject itemToDelete;
    
    // Update is called once per frame
    void Update()
    {

        if (!bought)
        {
            //Si no se ha comprado el producto, el collider de este estara desactivado para que el jugador no lo consiga gratuitamente
            product.GetComponent<CircleCollider2D>().enabled = false;
            texto.text = price + "";

        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.gameObject.tag == "Player")
        {

            if (Input.GetKeyDown(KeyCode.F))
            {

                if (!bought)
                {

                    if (collision.gameObject.GetComponent<playerController>().money >= price)
                    {
                        //Tras hacer la comprobacion de si el jugador tiene suficiente dinero, activa el producto y le resta el dinero al jugador
                        bought = true;
                        product.GetComponent<CircleCollider2D>().enabled = true;
                        texto.gameObject.SetActive(false);
                        cube.SetActive(false);
                        collision.gameObject.GetComponent<playerController>().money -= price;
                        collision.gameObject.GetComponent<playerController>().UpdateMoney();

                        if (objeto)
                        {
                            //En caso de que el producto sea un objeto, y este no sea el snack, se eliminara de la pool de objetos
                            if (product.name == "Snack(Clone)")
                            {

                            }
                            else
                            {
                                GameObject.Find("MainController").GetComponent<MainController>().itemPool.Remove(itemToDelete);
                            }
                        }

                    }

                }

            }

        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {

            if (Input.GetKeyDown(KeyCode.F))
            {

                if (!bought)
                {

                    if (collision.gameObject.GetComponent<playerController>().money >= price)
                    {

                        bought = true;
                        product.GetComponent<CircleCollider2D>().enabled = true;
                        texto.gameObject.SetActive(false);
                        cube.SetActive(false);
                        collision.gameObject.GetComponent<playerController>().money -= price;
                        collision.gameObject.GetComponent<playerController>().UpdateMoney();
                        if (objeto)
                        {
                            if (product.name == "Snack(Clone)")
                            {

                            }
                            else
                            {
                                GameObject.Find("MainController").GetComponent<MainController>().itemPool.Remove(itemToDelete);
                            }
                        }
                    }

                }

            }

        }

    }

}
