﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour
{
    //Pool de posibles drops en venta
    public List<GameObject> dropPool;
    //Objeto generador de items
    public GameObject itemSpot;
    //Spots disponibles sin producto
    public List<GameObject> availableSpots;
    //Spots con producto
    public List<GameObject> usedSpots;

    // Start is called before the first frame update
    void Start()
    {
        //Genera primero un objeto para asegurar que haya al menos uno en cada tienda
        GameObject firstItem = Instantiate(itemSpot, availableSpots[0].transform);
        firstItem.transform.localPosition = new Vector3(0, 0, 0);
        usedSpots.Add(availableSpots[0]);
        availableSpots.Remove(availableSpots[0]);
        GenerateShop();

    }

    private void GenerateShop()
    {
        
        int a = availableSpots.Count;

        for (int i = 0; i < a; i++)
        {
            //Calcula un numero aleatorio para decidir si se creara un drop o un objeto
            int rand = UnityEngine.Random.Range(0, 100);

            if(rand < 90)
            {

                GameObject firstItem = Instantiate(dropPool[UnityEngine.Random.Range(0, dropPool.Count)], availableSpots[0].transform);
                firstItem.transform.localPosition = new Vector3(0, 0, 0);
                availableSpots[0].GetComponent<ShopSpot>().product = firstItem;
                availableSpots[0].GetComponent<ShopSpot>().objeto = false;
                usedSpots.Add(availableSpots[0]);
                availableSpots.Remove(availableSpots[0]);

            }
            else
            {

                GameObject firstItem = Instantiate(itemSpot, availableSpots[0].transform);
                firstItem.transform.localPosition = new Vector3(0, 0, 0);
                
                usedSpots.Add(availableSpots[0]);
                availableSpots.Remove(availableSpots[0]);

            }

        }

    }
    
}
