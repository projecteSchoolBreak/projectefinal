﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpot : MonoBehaviour
{
    //Pool de objetos disponibles
    public List<GameObject> itemPool;
    //Controllador del main
    public MainController main;

    // Start is called before the first frame update
    void Start()
    {

        main = GameObject.Find("MainController").GetComponent<MainController>();
        itemPool = main.itemPool;
        Appearify();

    }
    //Funcion que genera los objetos para comprar
    public void Appearify()
    {
        //Crea un objeto aleatorio de la pool y destruye este gameObject
        int rand = Random.Range(0, itemPool.Count);
        GameObject newItem = Instantiate(itemPool[rand], this.transform.parent);
        this.transform.parent.gameObject.GetComponent<ShopSpot>().price = 6;
        this.transform.parent.gameObject.GetComponent<ShopSpot>().product = newItem;
        newItem.transform.position = this.transform.position;
        newItem.GetComponent<Objeto>().main = main;
        this.transform.parent.gameObject.GetComponent<ShopSpot>().itemToDelete = itemPool[rand];
        Destroy(this.gameObject);

    }

}
