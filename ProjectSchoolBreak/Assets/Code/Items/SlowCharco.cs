﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowCharco : MonoBehaviour
{
    //a los 2 segundos sera destruido
    void Start()
    {
        Invoke("Die", 2f);
    }

    //al pasar un enemigo por encima sufrira relentizacion
    void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Enemy")
        {

            collision.gameObject.GetComponent<enemy>().slowed();

        }

    }

    //al pasar un enemigo por encima sufrira relentizacion
    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Enemy")
        {

            collision.gameObject.GetComponent<enemy>().slowed();

        }

    }

    //destruye el charco
    void Die()
    {
        Destroy(this.gameObject);
    }

}
