﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre : MonoBehaviour
{
    //lista de items
    public List<GameObject> itemPool;
    public MainController main;

    private void Start()
    {
        main = GameObject.Find("MainController").GetComponent<MainController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            //si el personaje no tiene llaves no podra abrir el cofre
            if(collision.gameObject.GetComponent<playerController>().keys > 0)
            {
                //reducira nuestras lleves en 1
                collision.gameObject.GetComponent<playerController>().keys--;
                collision.gameObject.GetComponent<playerController>().UpdateKeys();
                //con un random creara un item de la lista
                int rand = Random.Range(0, itemPool.Count);
                GameObject newItem = Instantiate(itemPool[rand]);
                newItem.transform.position = this.transform.position;
                newItem.GetComponent<Objeto>().main = main;
                //si el item es el "snack" no lo eliminara de la lista, en cualquier otro caso si
                if (newItem.name == "Snack(Clone)")
                {

                }
                else
                {
                    main.itemPool.Remove(itemPool[rand]);
                }
                //destruye el cofre
                Destroy(this.gameObject);

            }

        }

    }

}
