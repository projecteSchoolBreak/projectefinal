﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objeto : MonoBehaviour
{
    //Informacion del objeto
    public Item itemData;
    //Controlador del main
    public MainController main;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {

            //HasBeenPicked();

        }

    }

    public void HasBeenPicked()
    {
        //Activa la funcion del main de la descripcion de los objetos
        Debug.Log("HasBeenPicked");
        main.GetItem(itemData);

    }

}
