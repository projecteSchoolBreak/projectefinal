﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Objeto")]
public class Item : ScriptableObject
{
    //nombre del objeto
    public string itemName;
    //descripcion del objeto
    public string itemDescription;

}
