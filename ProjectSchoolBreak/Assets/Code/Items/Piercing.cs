﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piercing : Objeto
{
    //Objeto jugador
    public playerController player;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            //Al colisionar con un jugador, lo convierte en su padre y llama a la funcion que activa sus efectos
            this.transform.parent = collision.transform;
            player = collision.gameObject.GetComponent<playerController>();
            OnPickUp();
            //Tambien llama a la funcion de la clase Objeto que activa el mensaje de descripcion
            HasBeenPicked();

        }

    }

    //Funcion que se llama al coger el objeto, y que aplica sus efectos
    void OnPickUp()
    {
        //Desactiva el sprite y el colisionador del objeto
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
        //hace que los disparos no se destruyan cuando colisionen con los enemigos
        player.gameObject.GetComponent<ataca>().piercing = true;

    }
}
