﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcetinSucio : Objeto
{

    public Familiar fam;
    //Objeto jugador
    public playerController player;
    //lista de diferentes imagenes de charcos
    public List<GameObject> charcos;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {

            //Al colisionar con un jugador, lo convierte en su padre y llama a la funcion que activa sus efectos
            this.transform.parent = collision.transform.parent;
            player = collision.gameObject.GetComponent<playerController>();
            OnPickUp();
            //Tambien llama a la funcion de la clase Objeto que activa el mensaje de descripcion
            HasBeenPicked();

        }

    }

    //Funcion que se llama al coger el objeto, y que aplica sus efectos
    void OnPickUp()
    {
        //reduce el tamano del item
        this.transform.localScale = new Vector3(.7f, .7f, 1f);
        this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
        //hacemos que el item siga a nuestro personaje
        fam.player = player.lastToFollow;
        player.lastToFollow = this.gameObject;
        fam.enabled = true;
        //soltara charcos cada segundo
        StartCoroutine(Spawn(1f));

    }

    IEnumerator Spawn(float time)
    {
        //cada segundo creara un charco en la posicion en la que este
        yield return new WaitForSeconds(time);
        //hara un random de las diferentes imagenes
        GameObject a = Instantiate(charcos[Random.Range(0, charcos.Count)]);
        a.transform.position = this.transform.position;
        StartCoroutine(Spawn(time));

    }

}
