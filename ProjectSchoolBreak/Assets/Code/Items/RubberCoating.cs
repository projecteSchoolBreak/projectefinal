﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubberCoating : Objeto
{
    //Objeto jugador
    public playerController player;
    //material que hace que rebote el disparo
    public PhysicsMaterial2D rubber;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Al colisionar con un jugador, lo convierte en su padre y llama a la funcion que activa sus efectos
        if (collision.gameObject.tag == "Player")
        {

            this.transform.parent = collision.transform;
            player = collision.gameObject.GetComponent<playerController>();
            OnPickUp();
            //Tambien llama a la funcion de la clase Objeto que activa el mensaje de descripcion
            HasBeenPicked();

        }

    }
    //Funcion que se llama al coger el objeto, y que aplica sus efectos
    void OnPickUp()
    {
        //Desactiva el sprite y el colisionador del objeto
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
        //activa el booleano para canviar variables
        player.gameObject.GetComponent<ataca>().rebote = true;
        //hace que nuestro disparo tenga el material
        player.gameObject.GetComponent<ataca>().materialRebote = rubber;

    }
}
