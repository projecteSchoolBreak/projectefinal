﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Familiar : MonoBehaviour
{

    private float lastFire;
    public GameObject player;
    private float lastOffsetX;
    private float lastOffsetY;
    public float speed = 5f;

    private void Start()
    {
        //player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (horizontal != 0 || vertical != 0)
        {
            float offsetX = (horizontal < 0) ? Mathf.Floor(horizontal) : Mathf.Ceil(horizontal);
            float offsetY = (vertical < 0) ? Mathf.Floor(vertical) : Mathf.Ceil(vertical);
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * (Time.deltaTime * 4));
            lastOffsetX = offsetX;
            lastOffsetY = offsetY;
        }
        else
        {

            if (!(transform.position.x < lastOffsetX + 1f) && !(transform.position.y < lastOffsetY + 1f))
            {
                //transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x - lastOffsetX, player.transform.position.y - lastOffsetY), speed * Time.deltaTime);
                transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * (Time.deltaTime * 4));
            }
            
        }
    }

}
