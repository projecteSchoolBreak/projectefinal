﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save
{
    //Booleanos que indican que personajes estan desbloqueados
    public bool unlockAtleta;
    public bool unlockAnimadora;
    public bool unlockGordo;
    public bool unlockAsiatico;

}
