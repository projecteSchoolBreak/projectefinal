﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GordoController : MonoBehaviour
{
    //Objeto del roll especial
    public GameObject dolorRoll;
    //Objeto del jugador
    public playerController itsAMe;
    //Booleano que indica si la habilidad esta disponible
    bool unavailable;
    //Velocidad del roll especial
    private float dashVel;
    //Estado actual del personaje
    private State state;
    private enum State
    {
        roll,
        rolling,
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.roll:
                roll();
                break;
            case State.rolling:
                rollin();
                break;
        }
    }

    void roll()
    {
        //Estado normal del personaje, que activa la habilidad especial si se pulsa la e
        if (Input.GetKey("e"))
        {
            if (!unavailable)
            {
                itsAMe.immobil = true;
                itsAMe.invulnerable = true;
                dolorRoll.SetActive(true);
                unavailable = true;
                StartCoroutine(FinSuper(itsAMe.cdAbility));
                state = State.rolling;
                dashVel = 10f;
            }
        }
    }
    //Funcion del super roll del personaje
    void rollin()
    {
        transform.position += itsAMe.lastDir * dashVel * Time.deltaTime;
        dashVel -= dashVel * 0.6f * Time.deltaTime;
        if (dashVel < 5f)
        {
            state = State.roll;
        }
    }
    //Corutina que desactiva los efectos del roll y reactiva la habilidad cuando pase el tiempo de enfriamiento
    IEnumerator FinSuper(float time)
    {
        yield return new WaitForSeconds(1);
        itsAMe.immobil = false;
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        itsAMe.invulnerable = false;
        dolorRoll.SetActive(false);
        yield return new WaitForSeconds(time);
        unavailable = false;
    }
}
