﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaController : MonoBehaviour
{
    //Objeto jugador
    public playerController player;
    //Sprite del corazon entero
    public Sprite fullHeart;
    //Sprite del medio corazon
    public Sprite halfHeart;
    //Sprite del corazon vacio
    public Sprite emptyHeart;
    //Objeto prefab de un corazon
    public GameObject objectFullHeart;
    float lastHealthyHeart;

    // Start is called before the first frame update
    void Start()
    {

        for (int i = 0; i < player.maxHealth/2f; i++)
        {
            Debug.Log("AAAAAAAAAAAAAAAAAAAAA");
            GameObject heart = Instantiate(objectFullHeart, this.transform);

        }
        lastHealthyHeart = (player.maxHealth / 2f) - 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (player.currHealth % 2 != 0)
        {
            lastHealthyHeart = ((player.currHealth + 1) / 2) - 1;
        }
        else
        {
            lastHealthyHeart = (player.currHealth / 2) - 1;
        }
    }
    //Funcion llamada cuando el jugador pierde vida
    public void lostHealth()
    {
        //Primero coge el ultimo corazon sano
        GameObject cor = this.transform.GetChild((int)lastHealthyHeart).gameObject;
        //Despues canvia el sprite del corazon en funcion de si era un corazon entero o medio corazon
        if(cor.GetComponent<Image>().sprite == fullHeart)
        {
            cor.GetComponent<Image>().sprite = halfHeart;
        }
        else if (cor.GetComponent<Image>().sprite == halfHeart)
        {
            cor.GetComponent<Image>().sprite = emptyHeart;
            if(lastHealthyHeart > 0)
            {
                lastHealthyHeart--;
            }
        }
    }
    //Funcion llamada cuando aumenta la vida maxima del personaje
    public void gainHealth(bool full)
    {
        if (full)
        {
            //Si aumentan tanto la vida maxima como la actual, recorre los corazones actuales para dejar los vacios y los medios corazones los ultimos
            GameObject cor = Instantiate(objectFullHeart, this.transform);
            for (int i = this.transform.childCount-1; i > 0; i--)
            {
                GameObject cors = this.transform.GetChild(i).gameObject;
                if (cors.GetComponent<Image>().sprite == emptyHeart)
                {
                    cors.GetComponent<Image>().sprite = fullHeart;
                    cor.GetComponent<Image>().sprite = emptyHeart;
                    cor = cors;
                }
                if (cors.GetComponent<Image>().sprite == halfHeart)
                {
                    cors.GetComponent<Image>().sprite = fullHeart;
                    cor.GetComponent<Image>().sprite = halfHeart;
                    cor = cors;
                }
            }
            lastHealthyHeart++;
        }
        else
        {
            //Si aumenta solo la vida maxima se anade un corazon vacio al final
            GameObject cor = Instantiate(objectFullHeart, this.transform);
            cor.GetComponent<Image>().sprite = emptyHeart;
            if(player.currHealth == player.maxHealth - 2)
            {
                lastHealthyHeart++;
            }
        }
    }
    //Funcion que se llama cuando el jugador recoge un corazon
    public void Recover()
    {
        GameObject cor = this.transform.GetChild((int)lastHealthyHeart).gameObject;
        if (cor.GetComponent<Image>().sprite == fullHeart)
        {
            lastHealthyHeart++;
            cor = this.transform.GetChild((int)lastHealthyHeart).gameObject;
        }
        if (cor.GetComponent<Image>().sprite == emptyHeart)
        {
            cor.GetComponent<Image>().sprite = halfHeart;
        }
        else if (cor.GetComponent<Image>().sprite == halfHeart)
        {
            cor.GetComponent<Image>().sprite = fullHeart;
            if(lastHealthyHeart < player.maxHealth)
            {
                lastHealthyHeart++;
            }
        }
    }
}
