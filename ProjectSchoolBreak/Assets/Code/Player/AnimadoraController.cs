﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimadoraController : MonoBehaviour
{
    //Posicion desde la que se lanza el beso
    public Transform pos;
    //Objeto jugador
    public playerController itsAMe;
    //Objeto prefab del beso
    public GameObject kiss;
    //Fuerza con la que se lanza el beso
    public float fuerza = 20f;
    //Booleano que indica si esta disponible la habilidad
    bool unavailable;

    // Update is called once per frame
    void Update()
    {
        //Habilidad que lanza un beso si esta disponible
        if (Input.GetKey("e"))
        {
            if (!unavailable)
            {
                disparo();
                unavailable = true;
                StartCoroutine(FinSuper(itsAMe.cdAbility));
            }
        }
    }
    //Funcion de disparo del beso
    void disparo()
    {
        GameObject lips = Instantiate(kiss, pos.position, pos.rotation);
        Rigidbody2D rb = lips.GetComponent<Rigidbody2D>();
        rb.AddForce(pos.up * fuerza, ForceMode2D.Impulse);
    }
    //Funcion que desbloquea la habilidad especial cuando pase el tiempo de enfriamiento
    IEnumerator FinSuper(float time)
    {
        yield return new WaitForSeconds(time);
        unavailable = false;
    }
}
