﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContenedorJugador : MonoBehaviour
{

    static GameObject player = null;

    void Awake()
    {
        //Patró singleton
        if (player == null)
        {
            //crea el objecte en el primer moment
            player = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena.
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(player);
        }
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }
    }
}
