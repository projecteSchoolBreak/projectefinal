﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class torreta : MonoBehaviour
{
    //Objeto prefab del proyectil
    public GameObject proy;
    //Fuerza de lanzamiento de los proyectiles
    public float fuerza = 5f;
    //Tiempo entre disparos
    public float cooldown = 2f;
    //Booleano que indica si la torreta esta recargando
    bool recharging;
    //Enemigo al que ataca la torreta
    GameObject enemigo;
    // Start is called before the first frame update
    void Start()
    {
        //Muerte automatica de la torreta
        Invoke("die", 5f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //Si un enemigo entra dentro del rango, la torreta lo atacara si no tiene un objetivo
        if(col.gameObject.tag == "Enemy")
        {
            if(!enemigo){
              enemigo = col.gameObject;
              mata();
            }
        }
    }

    void OnTriggerStay2D(Collider2D col){
        //Si el enemigo objetivo permanece en el area de efecto, la torreta seguira disparandole siempre que pueda
        if(col.gameObject == enemigo){
            mata();
        }
        else
        {
            //En caso de no tener objetivo, determinara el enemigo mas cercano como tal
            if (!enemigo)
            {
                if (col.gameObject.tag == "Enemy")
                {
                    enemigo = col.gameObject;
                    mata();
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D col){
        //Si el enemigo objetivo sale del area de efecto, se pierde el objetivo
        if(col.gameObject == enemigo){
            enemigo = null;
        }
    }

    void die()
    {
        Destroy(this.gameObject);
    }
    //Funcion de disparar a los enemigos
    void mata(){
      if(recharging)
      {

      }
      else
      {
        ataca(enemigo.transform);
        recharging = true;
        StartCoroutine(Recargar());
      }
    }
    //Funcion que crea los proyectiles
    void ataca(Transform objective)
    {
        GameObject bala = Instantiate(proy, this.transform.position, this.transform.rotation);
        Vector2 direccion = objective.position - this.transform.position;
        Rigidbody2D rb = bala.GetComponent < Rigidbody2D>();
        rb.AddForce(direccion*fuerza, ForceMode2D.Impulse);
    }
    //Corutina que recupera los disparos cuando haya pasado el tiempo de enfriamiento
    IEnumerator Recargar()
    {
        yield return new WaitForSeconds(cooldown);
        recharging = false;
    }
}
