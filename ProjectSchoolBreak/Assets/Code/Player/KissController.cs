﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KissController : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Al colisionar con un enemigo lo enamora
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<enemy>().charmed();
        }
        if(collision.gameObject.tag != "Player")
        {
            if (collision.gameObject.tag != "Ignored")
            {
                Destroy(gameObject);
            }
        }
    }
}
