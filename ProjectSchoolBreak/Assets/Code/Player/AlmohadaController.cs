﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlmohadaController : MonoBehaviour
{
    //Posibles imagenes del sustituto
    public List<Sprite> imagenes;
    //Objeto sustituto
    public GameObject dakimakura;
    //Vida maxima del sustituto
    public int maxhp;
    //Vida actual del sustituto
    public int curhp;
    //Duracion del sustituto
    public float duration;
    //Enemigos en el rango de efecto del sustituto
    public List<GameObject> enemies;
    //Objeto jugador
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        //Se coloca una imagen aleatoria en el sustituto y se iguala la vida actual con la maxima
        dakimakura.GetComponent<SpriteRenderer>().sprite = imagenes[Random.Range(0, imagenes.Count)];
        curhp = maxhp;
        //Se invoca la funcion de morir automaticamente
        Invoke("die", duration);

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //Todos los enemigos que entren en el radio de efecto del sustituto lo atacaran a el en lugar de al jugador
        if (col.gameObject.tag == "Enemy")
        {

            col.gameObject.GetComponent<AIDestinationSetter>().target = this.gameObject.transform;
            if (!enemies.Contains(col.gameObject))
            {
                enemies.Add(col.gameObject);
            }

        }

    }

    //Si los enemigos atacan al sustituto, pueden herirlo lo suficiente como para destruirlo antes de tiempo
    public void dolor(int damage)
    {

        curhp -= damage;

        if (curhp <= 0)
        {
            die();
        }

    }

    //Antes de ser destruido, los enemigos recuperaran el rastro del jugador
    void die()
    {

        foreach (GameObject e in enemies)
        {
            e.gameObject.GetComponent<AIDestinationSetter>().target = player.transform;
        }

        Destroy(this.gameObject);

    }

}
