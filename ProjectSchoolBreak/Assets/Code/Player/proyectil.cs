﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectil : MonoBehaviour
{
    //Dano del proyectil
    public int damage;
    //Propiedades del proyectil
    public bool slows, poisons, rebote, burns, paralyzes, piercing;
    //Contador de rebotes del proyectil
    int cuentaRebote = 0;
    //Direccion del disparo
    public Vector3 dir;

    void Start()
    {
        //Destruye el proyectil automaticamente tras 5 segundos
        Invoke("Die", 5f);
    }
    
    void Update()
    {
        //En el caso de proyectiles penetrante el collider se vuelve trigger
        if (piercing)
        {
            this.gameObject.GetComponent<Collider2D>().isTrigger = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Al colisionar con un enemigo, lo dana y le aplica los efectos
        if(collision.gameObject.tag == "Enemy"){
            collision.gameObject.GetComponent<enemy>().damageado(damage);
            if (slows)
            {
                collision.gameObject.GetComponent<enemy>().slowed();
            }
            if (poisons)
            {
                collision.gameObject.GetComponent<enemy>().poisoned(1);
            }
            if (burns)
            {
                collision.gameObject.GetComponent<enemy>().burned(1);
            }
            if (paralyzes)
            {
                collision.gameObject.GetComponent<enemy>().paralyzed();
            }
        }
        //En el caso de poder rebotar, se aumentara el contador hasta el limite antes de destruirse el proyectil
        if (rebote)
        {
            if(collision.gameObject.tag == "Muro")
            {
                if(cuentaRebote >= 2)
                {
                    Destroy(gameObject);
                }
                else
                {
                    cuentaRebote++;
                }
            }
            else
            {
                if (collision.gameObject.tag == "Proyectil" || collision.gameObject.tag == "Player")
                {

                }
                else
                {
                    Destroy(gameObject);
                }
            }
        }
        else
        {
            if (collision.gameObject.tag == "Proyectil" || collision.gameObject.tag == "Player")
            {

            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Enemy")
        {

            collision.gameObject.GetComponent<enemy>().damageado(damage);
            if (slows)
            {
                collision.gameObject.GetComponent<enemy>().slowed();
            }
            if (poisons)
            {
                collision.gameObject.GetComponent<enemy>().poisoned(1);
            }
            if (burns)
            {
                collision.gameObject.GetComponent<enemy>().burned(1);
            }
            if (paralyzes)
            {
                collision.gameObject.GetComponent<enemy>().paralyzed();
            }

        }

        if (rebote)
        {

            if (collision.gameObject.tag == "Muro")
            {

                if (cuentaRebote >= 2)
                {
                    Destroy(gameObject);
                }
                else
                {
                    cuentaRebote++;
                }

            }
            else
            {
                if (collision.gameObject.tag == "Proyectil" || collision.gameObject.tag == "Player")
                {

                }
                else
                {
                    //Destroy(gameObject);
                }
            }

        }
        else
        {

            if (collision.gameObject.tag == "Proyectil" || collision.gameObject.tag == "Player")
            {

            }
            else
            {
                if (collision.gameObject.tag == "Muro")
                {

                    Destroy(gameObject);

                }
            }

        }

    }

    public void Die()
    {
        Destroy(this.gameObject);
    }

}
