﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChineseController : MonoBehaviour
{
    //Objeto del jugador
    public playerController itsAMe;
    //Booleano que indica la disponibilidad de la habilidad especial
    bool unavailable;
    //Prefab del sustituto
    public GameObject dakima;

    // Update is called once per frame
    void Update()
    {

        //Habilidad sustituto
        if (Input.GetKey("e"))
        {
            //Al pulsar la e, y si esta disponible, se usara la habilidad especial
            if (!unavailable)
            {
                //Crea el objeto sustituto en la posicion del jugador
                GameObject newDakima = Instantiate(dakima);
                newDakima.transform.position = this.transform.position;
                newDakima.GetComponent<AlmohadaController>().player = this.gameObject;
                //Despues de usarse, empieza el tiempo de enfriamiento de la habilidad especial
                unavailable = true;
                StartCoroutine(FinSuper(itsAMe.cdAbility));

            }


        }

    }

    //Corutina que reactiva la habilidad especial despues del tiempo de enfriamiento
    IEnumerator FinSuper(float time)
    {

        yield return new WaitForSeconds(time);
        unavailable = false;

    }

}
