﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectilTorreta : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Al colisionar con un enemigo lo dana
        if(collision.gameObject.tag == "Enemy"){
            collision.gameObject.GetComponent<enemy>().damageado(1);
        }
        Destroy(gameObject);
    }
}
