﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeportistaController : MonoBehaviour
{
    //Objeto jugador
    public playerController itsAMe;
    //Booleano que marca la disponibilidad de la habilidad
    bool unavailable;
    //Duracion de la habilidad
    public float duration;
    
    // Update is called once per frame
    void Update()
    {
        //Habilidad que hace invulnerable al personaje si esta disponible
        if (Input.GetKey("e"))
        {
            if (!unavailable)
            {
                unavailable = true;
                itsAMe.invulnerable = true;
                itsAMe.GetComponent<SpriteRenderer>().color = Color.red;
                StartCoroutine(Invulnerabilidad(duration));
                StartCoroutine(FinSuper(itsAMe.cdAbility));
            }
        }
    }
    //Corutina que desactiva la invulnerabilidad y devuelve al jugador su color normal
    IEnumerator Invulnerabilidad(float time)
    {
        yield return new WaitForSeconds(time);
        itsAMe.invulnerable = false;
        itsAMe.GetComponent<SpriteRenderer>().color = Color.white;
    }
    //Corutina que desbloquea la habilidad cuando ha pasado el tiempo de enfriamiento
    IEnumerator FinSuper(float time)
    {
        yield return new WaitForSeconds(time);
        unavailable = false;
    }
}
