﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class playerController : MonoBehaviour
{
    //Algun cambio
    //Animador del jugador
    private Animator anim;
    //Camara de la escena
    public Camera gameCamera;
    //Objeto que controla los distintos corazones de la interficie
    public VidaController vida;
    //Texto de las monedas que tiene el jugador
    public Text coinsText;
    //Texto de las llaves que tiene el jugador
    public Text keysText;
    //Canvas de la interficie
    public GameObject canvas;
    //Ventana de carga
    public GameObject loadingScreen;
    //Objeto con las estrellas animadas del estado de confusion
    public GameObject estrellas;

    [Header("Stats")]
    //Vida maxima del jugador
    public int maxHealth = 6;
    //Actual vida del jugador
    public int currHealth;
    //Velocidad actual del jugador
    public float vel;
    //Velocidad original del jugador
    public float originalVel;
    //Dinero que posee el jugador
    public int money = 0;
    //Llaves que posee el jugador
    public int keys = 0;
    //Estado del jugador
    public State state;
    /*
     * 
     * Los diferentes estados son:
     * Normal: estado base del jugador
     * Dasheando: estado del jugador al hacer el sprint
     * Confuso: estado que invierte los controles de movimiento del jugador
     * Slow: estado que ralentiza el movimiento del jugador
     * 
    */
    public enum State
    {
        Normal,
        Dasheando,
        Confuso,
        Slow,
    }

    [HideInInspector]
    //Booleanos que indican si el jugador es invulnerable o esta inmovilizado
    public bool invulnerable, immobil;
    //Objeto que seguiran los familiares, en caso de tener mas de uno
    public GameObject lastToFollow;
    //Tiempo de reutilizacion de la habilidad especial del personaje
    public float cdAbility;
    //Posicion del raton
    Vector2 mousePos;
    //Ultima direccion en la que se movio el jugador
    public Vector3 lastDir;
    //Lista de objetos recogidos por el jugador
    public List<Item> itemsPasivos;
    //Velocidad del sprint del jugador
    private float dashVel;
    //Direccion actual del jugador
    public int intdir;
    //Booleano que indica si ya se ha desbloqueado un personaje en la partida actual
    bool unlockChar = true;
    //El sprite del personaje
    public SpriteRenderer sprites;


    // Start is called before the first frame update
    void Start()
    {

        /*
         * 
         * Al empezar, nos aseguramos de que la variable anim tenga el animador del personaje,
         * que la vida actual sea la misma que la maxima
         * y que la velocidad original sea la misma que la actual
         * 
        */

        sprites = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        currHealth = maxHealth;
        originalVel = vel;

        //Buscamos la camara que sera hija del objeto main controller
        gameCamera = GameObject.Find("MainController").GetComponent<MainController>().gameCamera;
        //Decimos que este objeto sera el primero en seguirse por los familiares
        lastToFollow = this.gameObject;
        //Actualizamos los contadores de llaves y de dinero
        UpdateKeys();
        UpdateMoney();

    }

    // Update is called once per frame
    void Update()
    {
        //En funcion del estado actual del jugador llamamos a distintas funciones de movimiento
        switch (state)
        {
            case State.Normal:
                movimiento();
                dash();
                break;
            case State.Dasheando:
                dasheo();
                break;
            case State.Confuso:
                confuso();
                break;
            case State.Slow:
                slowed();
                break;
        }

        //Si el jugador ha conseguido superar dos niveles se desbloqueara un nuevo personaje, en caso de no haber desbloqueado uno ya
        if(MainController.nivel > 2 && unlockChar)
        {

            unlockChar = false;

            MainController.UnlockNewChar();

        }
        //Nos aseguramos de que la camara siga constantemente la posicion del jugador
        gameCamera.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -10f);
    }


    //Funcion de movimiento en el caso del estado normal del jugador
    void movimiento()
    {
        //Variables que indican el movimiento horizontal y vertical
        float moveX = 0f;
        float moveY = 0f;

        //En caso de no estar inmovilizado el jugador podra moverse sin problema
        if (!immobil)
        {

            //Segun la tecla pulsada en el momento, se añaden los valores necesarios a la variables de movimiento

            if (Input.GetKey(KeyCode.W))
            {
                moveY = +1f;
                intdir = 1;
            }

            if (Input.GetKey(KeyCode.S))
            {
                moveY = -1f;
                intdir = 4;
            }

            if (Input.GetKey(KeyCode.A))
            {
                moveX = -1f;
                intdir = 2;
            }

            if (Input.GetKey(KeyCode.D))
            {
                moveX = +1f;
                intdir = 3;
            }

            //Se le dice al animador el movimiento actual y la direccion
            anim.SetFloat("moveX", moveX);
            anim.SetFloat("moveY", moveY);
            anim.SetInteger("Dir", intdir);
            //Se crea el vector de movimiento y se actualiza la posicion del jugador
            Vector3 moveDir = new Vector3(moveX, moveY).normalized;
            lastDir = moveDir;
            transform.position += moveDir * vel * Time.deltaTime;
        }

    }

    //Funcion llamada para controlar si el jugador activa el dash
    public void dash()
    {
        
        //En caso de pulsar la tecla espacio, se activara el dash y se cambiara el estado del jugador
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger("Dash");
            invulnerable = true;
            StartCoroutine(RecuperarMobilidad(0.3f));

            state = State.Dasheando;
            dashVel = 50f;
        }

    }

    //Funcion de movimiento en el caso del estado dasheando del jugador
    void dasheo()
    {

        //Se actualiza la posicion del jugador segun la ultima direccion en la que se movio
        transform.position += lastDir * dashVel * Time.deltaTime;
        
        //Se ralentiza gradualmente la velocidad del dash, y finalmente se devuelve al jugador a su estado normal
        dashVel -= dashVel * 10f * Time.deltaTime;
        if (dashVel < 5f)
        {
            state = State.Normal;
        }
    }

    //Funcion de daño al jugador
    public void dolor(int damage)
    {
        
        //En caso de no ser invulnerable, se reducira la vida actual del jugador
        //y se actualizara la vida de la interficie
        if (!invulnerable)
        {
            sprites.color = Color.white;
            currHealth -= damage;

            for (int i = 0; i < damage; i++)
            {
                vida.lostHealth();
            }

            //Al ser dañado, el jugador se volvera invulnerable durante un breve periodo de tiempo
            invulnerable = true;
            StartCoroutine(Invulnerability(0.5f));

        }

        //En el caso de que la vida actual sea 0 o menor, se llamara a la funcion de muerte del jugador
        if (currHealth <= 0)
        {
            muerte();
        }

    }

    //Funcion que anade corazones nuevos en la interfaz al aumentar la vida maxima
    public void UpdateHealth(bool full)
    {

        //Se envia una variable booleana que indica si el nuevo corazon esta lleno o no
        vida.gainHealth(full);

    }

    //Funcion de muerte del jugador
    void muerte()
    {
        //Destruye el objeto jugador y nos envia a la escena de "muerte"
        SceneManager.LoadScene("GameOver");
        Destroy(this.transform.parent.gameObject);
    }

    //Funcion llamada cuando un ataque enemigo impacta al jugador y lo confunde
    public void confusion()
    {
        //Cambia el estado a confuso de forma temporal y activa el objeto de estrellas
        state = State.Confuso;
        estrellas.SetActive(true);
        StartCoroutine(Recover(2f));
    }

    //Funcion llamada cuando un ataque enemigo impacta al jugador y lo ralentiza
    public void slow()
    {
        //Cambia el estado a slow de forma temporal y reduce la velocidad del jugador
        state = State.Slow;
        vel = originalVel * 0.8f;
        StartCoroutine(Recover(2f));
    }

    //Funcion de movimiento en el caso del estado slow del jugador
    void slowed()
    {

        //Similar a la funcion de movimiento del estado normal, pero con valores mas bajos

        float moveX = 0f;
        float moveY = 0f;

        if (!immobil)
        {
            if (Input.GetKey(KeyCode.W))
            {
                moveY = +1f;
            }

            if (Input.GetKey(KeyCode.S))
            {
                moveY = -1f;
            }

            if (Input.GetKey(KeyCode.A))
            {
                moveX = -1f;
            }

            if (Input.GetKey(KeyCode.D))
            {
                moveX = +1f;
            }

            Vector3 moveDir = new Vector3(moveX, moveY).normalized;
            lastDir = moveDir;
            transform.position += moveDir * vel * Time.deltaTime;
        }

    }

    //Funcion de movimiento en el caso del estado confuso del jugador
    void confuso()
    {

        //Funciona igual que el estado normal, pero invierte los controles en cada eje

        float moveX = 0f;
        float moveY = 0f;

        if (!immobil)
        {
            if (Input.GetKey(KeyCode.W))
            {
                moveY = -1f;
            }

            if (Input.GetKey(KeyCode.S))
            {
                moveY = +1f;
            }

            if (Input.GetKey(KeyCode.A))
            {
                moveX = +1f;
            }

            if (Input.GetKey(KeyCode.D))
            {
                moveX = -1f;
            }


        Vector3 moveDir = new Vector3(moveX, moveY).normalized;
        lastDir = moveDir;
        transform.position += moveDir * vel * Time.deltaTime;
        }
    }

    //Funcion que actualiza el contador de monedas de la interfaz
    public void UpdateMoney()
    {

        if(money < 10)
        {

            coinsText.text = "0" + money;

        }
        else
        {

            coinsText.text = money+"";

        }

    }

    //Funcion que actualiza el contador de llaves de la interfaz
    public void UpdateKeys()
    {

        if (keys < 10)
        {

            keysText.text = "0" + keys;

        }
        else
        {

            keysText.text = keys + "";

        }

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        
        //En el caso de colisionar con un objeto corazon, curara al jugador dos puntos de vida, si es posible
        if(collision.gameObject.tag == "Heart")
        {

            for (int i = 0; i < 2; i++)
            {

                if(currHealth < maxHealth)
                {

                    vida.Recover();
                    currHealth++;

                }

            }

            Destroy(collision.gameObject);

        }

        //En el caso de colisionar con un objeto medio corazon, curara al jugador un punto de vida, si es posible
        if (collision.gameObject.tag == "HalfHeart")
        {

            for (int i = 0; i < 1; i++)
            {

                if (currHealth < maxHealth)
                {

                    vida.Recover();
                    currHealth++;

                }

            }

            Destroy(collision.gameObject);

        }

        //En el caso de colisionar con un objeto moneda, aumentara la cantidad de monedas del jugador y actualizara la interfaz
        if (collision.gameObject.tag == "Coin")
        {

            money++;
            Destroy(collision.gameObject);
            UpdateMoney();

        }

        //En el caso de colisionar con un objeto llave, aumentara la cantidad de llaves del jugador y actualizara la interfaz
        if (collision.gameObject.tag == "Key")
        {

            keys++;
            Destroy(collision.gameObject);
            UpdateKeys();

        }

        //En el caso de colisionar con un objeto escaleras, desactivara la interfaz y activara la pantalla de carga
        if (collision.gameObject.tag == "Stairs")
        {

            canvas.SetActive(false);
            loadingScreen.SetActive(true);

        }

    }

    //Corutina que desactivara la invulnerabilidad cuando pase el tiempo especificado

    IEnumerator Invulnerability(float time)
    {
        yield return new WaitForSeconds(0.1f);
        sprites.color = Color.white;
        yield return new WaitForSeconds(0.1f);
        sprites.color = Color.grey;
        yield return new WaitForSeconds(0.1f);
        sprites.color = Color.white;
        yield return new WaitForSeconds(0.1f);
        sprites.color = Color.grey;
        yield return new WaitForSeconds(0.1f);
        invulnerable = false;
        sprites.color = Color.white;
    }

    //Corutina que desactivara las estrellas y devolvera al jugador su velocidad original y su estado normal
    IEnumerator Recover(float time)
    {
        yield return new WaitForSeconds(time);
        estrellas.SetActive(false);
        state = State.Normal;
        vel = originalVel;
    }

    //Corutina que desactivara la invulnerabilidad cuando pase el tiempo especificado
    IEnumerator RecuperarMobilidad(float time)
    {
        yield return new WaitForSeconds(time);
        invulnerable = false;
    }

}

