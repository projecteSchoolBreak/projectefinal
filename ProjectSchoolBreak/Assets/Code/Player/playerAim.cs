﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAim : MonoBehaviour
{
    //gameobject vacio que se usara para apuntar
    private Transform aimTransform;

    //buscamos el gameobject y se lo asignamos a la variable
    private void Awake()
    {
        aimTransform = transform.Find("Aim");
    }

    //actualizamos la posicion del objeto para que siempre apunte a donde se situa nuestro cursor
    private void Update()
    {
        Vector3 mousePos = GetMouseWorldPosition();

        Vector3 aimDir = (mousePos - transform.position).normalized;
        //ajustamos el angulo para que pase a grados
        float angle = Mathf.Atan2(aimDir.y, aimDir.x) * Mathf.Rad2Deg;
        aimTransform.eulerAngles = new Vector3(0, 0, angle);
        //Debug.Log(angle);
    }

    //funciones para encontrar el cursor en pantalla
    public static Vector3 GetMouseWorldPosition()
    {
        Vector3 vec = GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
        vec.z = 0f;
        return vec;
    }
    public static Vector3 GetMouseWorldPositionWithZ()
    {
        return GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
    }
    public static Vector3 GetMouseWorldPositionWithZ(Camera worldCamera)
    {
        return GetMouseWorldPositionWithZ(Input.mousePosition, worldCamera);
    }
    public static Vector3 GetMouseWorldPositionWithZ(Vector3 screenPosition, Camera worldCamera)
    {
        Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }
}
