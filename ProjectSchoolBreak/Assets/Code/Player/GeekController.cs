﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeekController : MonoBehaviour
{
    //Prefab de la torreta
    public GameObject torreta;
    //Objeto jugador
    public playerController itsAMe;
    //Booleano que indica si la habilidad esta disponible
    bool unavailable;

    // Update is called once per frame
    void Update()
    {
        //Habilidad plantar torretas
        if (Input.GetKey("e"))
        {
            if (!unavailable)
            {
                //Si la habilidad esta disponible, se creara una torreta en la posicion del jugador
                GameObject newTurret = Instantiate(torreta);
                newTurret.transform.position = this.transform.position;
                unavailable = true;
                StartCoroutine(FinSuper(itsAMe.cdAbility));
            }
        }
    }
    //Corutina que recupera la habilidad cuando pasa el tiempo de enfriamiento
    IEnumerator FinSuper(float time)
    {
        yield return new WaitForSeconds(time);
        unavailable = false;
    }
}
