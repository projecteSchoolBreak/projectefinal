﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ataca : MonoBehaviour
{
    //Transform del objeto des de el que se ataca
    public Transform pos;
    //Animador del personaje, usado en los golpes cuerpo a cuerpo
    public Animator animGolpe;
    //Objeto prefab de los proyectiles disparados
    public GameObject proy;
    //Fuerza en el disparo de los proyectiles
    public float fuerza = 20f;
    //Rango de los golpes cuerpo a cuerpo
    public float rango = 0.5f;
    //Layer donde se encuentran todos los enemigos
    public LayerMask layerEnemigo;

    //Dano de los ataques del jugador
    public int attackDamage = 1;
    //Numero de disparos del jugador al atacar
    public int nDisparos = 1;
    //Tiempo de espera entre disparos
    public float fireRate = 1f;
    //Booleano que indica si los ataques ralentizan a los enemigos
    public bool slows = false;
    //Booleano que indica si los ataques envenenan a los enemigos
    public bool poisons = false;
    //Booleano que indica si los ataques queman a los enemigos
    public bool burns = false;
    //Booleano que indica si los ataques paralizan a los enemigos
    public bool paralyzes = false;
    //Booleano que indica que el jugador esta recargando su ataque
    public bool recharging = false;
    //Booleano que indica si los ataques rebotan
    public bool rebote = false;
    //Booleano que indica si los ataques penetran
    public bool piercing = false;
    //Material de las fisicas de rebote
    public PhysicsMaterial2D materialRebote;

    // Update is called once per frame
    void Update()
    {

        //En caso de no estar recargando, el jugador podra atacar
        if (!recharging)
        {
            //El click izquierdo del raton se usa para los ataques a distancia,
            //y el click derecho se usa para los ataques cuerpo a cuerpo
            if (Input.GetButton("Fire1"))
            {
                disparo();
            }
            else if (Input.GetButton("Fire2"))
            {
                melee();
            }
            //Tras atacar se indicara que se debe recargar, y se desactivar el ataque temporalmente
            recharging = true;
            StartCoroutine(Recharge());

        }
        
    }

    void disparo()
    {

        if (nDisparos % 2 == 0)
        {

            //En caso de tener un numero par de disparos, se posicionan correctamente antes de ser disparados con la fuerza especificada
            //A cada proyectil se le envian los booleanos correspondientes a las caracteristicas del ataque, ya sea que envenene o ralentice...

            for (int i = 0; i < nDisparos/2; i++)
            {

                GameObject bala1 = Instantiate(proy, pos.position, pos.rotation);
                bala1.GetComponent<proyectil>().damage = attackDamage;
                bala1.GetComponent<proyectil>().slows = slows;
                bala1.GetComponent<proyectil>().poisons = poisons;
                bala1.GetComponent<proyectil>().rebote = rebote;
                bala1.GetComponent<proyectil>().burns = burns;
                bala1.GetComponent<proyectil>().paralyzes = paralyzes;
                bala1.GetComponent<proyectil>().piercing = piercing;
                if (rebote)
                {
                    bala1.GetComponent<Collider2D>().sharedMaterial = materialRebote;
                }
                Rigidbody2D rb1 = bala1.GetComponent<Rigidbody2D>();
                rb1.AddForce(pos.up * fuerza, ForceMode2D.Impulse);

                if(pos.up.x < 0.5f && pos.up.x > (-0.5f))
                {
                    bala1.transform.position = new Vector3(bala1.transform.position.x - (i * 0.75f), bala1.transform.position.y, bala1.transform.position.z);
                }
                else
                {
                    bala1.transform.position = new Vector3(bala1.transform.position.x, bala1.transform.position.y - (i * 0.75f), bala1.transform.position.z);
                }
                

                GameObject bala2 = Instantiate(proy, pos.position, pos.rotation);
                bala2.GetComponent<proyectil>().damage = attackDamage;
                bala2.GetComponent<proyectil>().slows = slows;
                bala2.GetComponent<proyectil>().poisons = poisons;
                bala2.GetComponent<proyectil>().rebote = rebote;
                bala2.GetComponent<proyectil>().burns = burns;
                bala2.GetComponent<proyectil>().paralyzes = paralyzes;
                bala2.GetComponent<proyectil>().piercing = piercing;
                if (rebote)
                {
                    bala2.GetComponent<Collider2D>().sharedMaterial = materialRebote;
                }
                Rigidbody2D rb2 = bala2.GetComponent<Rigidbody2D>();
                rb2.AddForce(pos.up * fuerza, ForceMode2D.Impulse);

                if (pos.up.x < 0.5f && pos.up.x > (-0.5f))
                {
                    bala2.transform.position = new Vector3(bala2.transform.position.x + (i * 0.75f), bala2.transform.position.y, bala2.transform.position.z);
                }
                else
                {
                    bala2.transform.position = new Vector3(bala2.transform.position.x, bala2.transform.position.y + (i * 0.75f), bala2.transform.position.z);
                }


            }

        }
        else
        {

            //En caso de tener un numero impar de disparos, se generaria un primer proyectil en el centro, y luego se generaria el resto de forma simetrica
            //De nuevo, se envia a cada proyectil las caracteristicas del ataque

            GameObject bala = Instantiate(proy, pos.position, pos.rotation);
            bala.GetComponent<proyectil>().damage = attackDamage;
            bala.GetComponent<proyectil>().slows = slows;
            bala.GetComponent<proyectil>().poisons = poisons;
            bala.GetComponent<proyectil>().rebote = rebote;
            bala.GetComponent<proyectil>().burns = burns;
            bala.GetComponent<proyectil>().paralyzes = paralyzes;
            bala.GetComponent<proyectil>().piercing = piercing;
            if (rebote)
            {
                //En el caso de que los proyectiles reboten, se le envia tambien el material necesario para que funcione correctamente
                bala.GetComponent<Collider2D>().sharedMaterial = materialRebote;
            }
            Rigidbody2D rb = bala.GetComponent<Rigidbody2D>();
            rb.AddForce(pos.up * fuerza, ForceMode2D.Impulse);

            if (nDisparos > 1)
            {
                for (int i = 0; i < (nDisparos - 1) / 2; i++)
                {

                    GameObject bala1 = Instantiate(proy, pos.position, pos.rotation);
                    bala1.GetComponent<proyectil>().damage = attackDamage;
                    bala1.GetComponent<proyectil>().slows = slows;
                    bala1.GetComponent<proyectil>().poisons = poisons;
                    bala1.GetComponent<proyectil>().rebote = rebote;
                    bala1.GetComponent<proyectil>().burns = burns;
                    bala1.GetComponent<proyectil>().paralyzes = paralyzes;
                    bala1.GetComponent<proyectil>().piercing = piercing;
                    if (rebote)
                    {
                        bala1.GetComponent<Collider2D>().sharedMaterial = materialRebote;
                    }
                    Rigidbody2D rb1 = bala1.GetComponent<Rigidbody2D>();
                    rb1.AddForce(pos.up * fuerza, ForceMode2D.Impulse);

                    if (pos.up.x < 0.5f && pos.up.x > (-0.5f))
                    {
                        bala1.transform.position = new Vector3(bala1.transform.position.x - (i * 0.75f), bala1.transform.position.y, bala1.transform.position.z);
                    }
                    else
                    {
                        bala1.transform.position = new Vector3(bala1.transform.position.x, bala1.transform.position.y - (i * 0.75f), bala1.transform.position.z);
                    }


                    GameObject bala2 = Instantiate(proy, pos.position, pos.rotation);
                    bala2.GetComponent<proyectil>().damage = attackDamage;
                    bala2.GetComponent<proyectil>().slows = slows;
                    bala2.GetComponent<proyectil>().poisons = poisons;
                    bala2.GetComponent<proyectil>().rebote = rebote;
                    bala2.GetComponent<proyectil>().burns = burns;
                    bala2.GetComponent<proyectil>().paralyzes = paralyzes;
                    bala2.GetComponent<proyectil>().piercing = piercing;
                    if (rebote)
                    {
                        bala2.GetComponent<Collider2D>().sharedMaterial = materialRebote;
                    }
                    Rigidbody2D rb2 = bala2.GetComponent<Rigidbody2D>();
                    rb2.AddForce(pos.up * fuerza, ForceMode2D.Impulse);

                    if (pos.up.x < 0.5f && pos.up.x > (-0.5f))
                    {
                        bala2.transform.position = new Vector3(bala2.transform.position.x + (i * 0.75f), bala2.transform.position.y, bala2.transform.position.z);
                    }
                    else
                    {
                        bala2.transform.position = new Vector3(bala2.transform.position.x, bala2.transform.position.y + (i * 0.75f), bala2.transform.position.z);
                    }

                }
            }
            

        }

        
    }

    void melee()
    {
        //Al atacar cuerpo a cuerpo se activa tambien su animacion
        animGolpe.SetTrigger("golpe");
        //Se crea una lista con todos los objetos golpeados dentro del rango y en la layer de los enemigos
        Collider2D[] enemigosGolpeados = Physics2D.OverlapCircleAll(pos.position, rango, layerEnemigo);
        //Cada elemento de la lista sera danado y se le aplicaran los efectos del ataque
        foreach(Collider2D enemy in enemigosGolpeados)
        {

            if(enemy.gameObject.tag == "Enemy")
            {

                enemy.GetComponent<enemy>().damageado(attackDamage);
                if (slows)
                {
                    enemy.GetComponent<enemy>().slowed();
                }
                if (poisons)
                {
                    enemy.GetComponent<enemy>().poisoned(1);
                }
                if (burns)
                {
                    enemy.GetComponent<enemy>().burned(1);
                }
                if (paralyzes)
                {
                    enemy.GetComponent<enemy>().paralyzed();
                }

            }
            
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(pos.position, rango);
    }

    //Corutina que recupera los ataques una vez pasado el tiempo necesario
    IEnumerator Recharge()
    {

        yield return new WaitForSeconds(fireRate);
        recharging = false;

    }

}
