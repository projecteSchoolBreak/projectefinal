﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DummyBoss : MonoBehaviour
{

    /*
     * 
     * Esta es la clase en la que esta todo el codigo referente a las preguntas
     * y la conversacion con los bosses antes de pelear contra ellos.
     *
    */

    //Lista de todas las preguntas que puede hacer el boss
    public List<Question> preguntas;
    public MainController main;
    int preguntaElegida;
    int respuestaCorrecta;
    int respuestaIncorrecta = 0;
    public int playerAnswer;
    public RoomController myRoom;


    // Start is called before the first frame update
    void Start()
    {

        //Encuentra el objeto main y le da este objeto a su variable dummy
        main = GameObject.Find("MainController").GetComponent<MainController>();
        main.dummy = this;

        //Escoge una pregunta de la lista de forma aleatoria
        preguntaElegida = Random.Range(0, preguntas.Count);
        //Edita el texto para mostrar la pregunta escogida
        main.pregunta.text = preguntas[preguntaElegida].pregunta;
        //Escoge la posicion de la respuesta correcta
        respuestaCorrecta = Random.Range(0, 4);

        //Posiciona la respuesta correcta y las incorrectas en los distintos botones
        for (int i = 0; i < 4; i++)
        {

            if(i == respuestaCorrecta)
            {

                main.respuestas[i].text = preguntas[preguntaElegida].respuestaCorrecta;

            }
            else
            {

                main.respuestas[i].text = preguntas[preguntaElegida].respuestasFalsas[respuestaIncorrecta];
                respuestaIncorrecta++;

            }

        }

        //Se asegura de activar el texto de las preguntas y las respuestas para mostrarselos al jugador
        main.pregunta.gameObject.SetActive(true);
        foreach (Text t in main.respuestas)
        {

            t.gameObject.SetActive(true);

        }
        foreach (GameObject b in main.botones)
        {

            b.SetActive(true);

        }
        main.fondoPregunta.SetActive(true);

    }

    //Funcion que comprueba si la respuesta del jugador es correcta o no
    public void Comprobar()
    {

        if(respuestaCorrecta == playerAnswer)
        {

            //En el caso de ser correcta se desactivaran los textos y se le dira al controlador que el boss no recibe ninguna bonificacion

            main.PlayCorrectAnswer();

            main.pregunta.gameObject.SetActive(false);
            foreach (Text t in main.respuestas)
            {

                t.gameObject.SetActive(false);

            }
            foreach (GameObject b in main.botones)
            {

                b.SetActive(false);

            }
            main.fondoPregunta.SetActive(false);
            myRoom.BuffarBoss(false);

        }
        else
        {

            //En el caso de ser incorrecta se desactivaran los textos y se le dira al controlador que el boss recibe una bonificacion
            main.PlayBadAnswer();

            main.pregunta.gameObject.SetActive(false);
            foreach (Text t in main.respuestas)
            {

                t.gameObject.SetActive(false);

            }
            foreach (GameObject b in main.botones)
            {

                b.SetActive(false);

            }
            main.fondoPregunta.SetActive(false);
            myRoom.BuffarBoss(true);

        }

    }

}
