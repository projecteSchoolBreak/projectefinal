﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuerdaController : MonoBehaviour
{
    //Punto sobre el que girara la cuerda
    public Vector3 eje;

    // Update is called once per frame
    void Update()
    {
        //Hacemos que la cuerda gire continuamente
        transform.RotateAround(eje, Vector3.back, 3f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //En el caso de golpear al jugador o al sustituto les hara dano
        if (col.gameObject.tag == "Player") col.gameObject.GetComponent<playerController>().dolor(1);
        if (col.gameObject.tag == "Substitute") col.gameObject.GetComponent<AlmohadaController>().dolor(1);
    }

}
