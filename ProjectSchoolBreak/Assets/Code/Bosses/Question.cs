﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pregunta")]
public class Question : ScriptableObject
{
    //Texto de la pregunta
    public string pregunta;
    //Texto de la respuesta correcta
    public string respuestaCorrecta;
    //Lista de las respuestas incorrectas
    public List<string> respuestasFalsas;

}
