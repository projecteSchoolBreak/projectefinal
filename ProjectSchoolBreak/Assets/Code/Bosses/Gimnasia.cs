﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gimnasia : MonoBehaviour
{

    //Objeto prefab del balon de volleyball
    public GameObject balon;
    //Objeto prefab de la cuerda
    public GameObject cuerda;
    //Jugador
    public Transform player;
    //Objetivo que persigue el boss
    public Transform target;
    //Booleano que decide si ataca o no el boss
    public bool attacking;
    //Dano del boss
    public int damage;
    //Posiciones entre las que se mueve el boss
    public float minX, maxX, minY, maxY;
    //Fuerza con la que dispara los proyectiles el boss
    public float fuerza;
    //Tiempo de enfriamiento entre ataques
    public float cd;
    //Objeto del boss
    public GameObject yo;

    // Start is called before the first frame update
    void Start()
    {
        attacking = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (attacking)
        {
            //Si el boss esta atacando hara una de sus tres acciones de forma aleatoria
            //El boss tiene un 50% de probabilidades de lanzar un balonazo, un 25% de moverse por la habitacion y un 25% de atacar con la cuerda
            int accion = Random.Range(0, 100);

            if(accion < 50)
            {
                balonazo();
            }
            else if(accion < 75)
            {
                movimiento();
            }
            else
            {
                cuerdaAtk();
            }

        }


    }
    //Funcion que crea un objeto balon y lo lanza hacia el jugador
    void balonazo()
    {

        attacking = false;

        Vector2 dir = player.position - this.transform.position;

        GameObject bala = Instantiate(balon, new Vector2(this.transform.position.x + (dir.x * 0.3f), this.transform.position.y + (dir.y * 0.3f)), this.transform.rotation);
        bala.GetComponent<EnemyProyectil>().damage = damage;
        Rigidbody2D rb = bala.GetComponent<Rigidbody2D>();
        rb.AddForce(dir * fuerza, ForceMode2D.Impulse);

        StartCoroutine(Recargar());

    }
    
    void cuerdaAtk()
    {
        //Al usar el ataque con la cuerda, desactivamos el booleano para que no realice otras acciones y bloqueamos el movimiento
        attacking = false;
        yo.GetComponent<AILerp>().canMove = false;
        //Creamos la cuerda en la posicion establecida y llamamos a la corutina que termina la accion
        GameObject rope = Instantiate(cuerda, this.transform);
        rope.transform.position = new Vector2(this.transform.position.x, this.transform.position.y - 4f);
        rope.transform.localScale = new Vector3(0.1f, 2f, 1f);
        rope.GetComponent<CuerdaController>().eje = this.transform.position;
        StartCoroutine(EndRope(rope, 2.4f));

    }
    //Funcion del movimiento del boss
    void movimiento()
    {
        //Desactiva el ataque y mueve el target que perseguira el boss
        attacking = false;
        Vector3 newPosition = new Vector3(Random.Range(minX,maxX), Random.Range(minY, maxY), 0);
        target.localPosition = newPosition;
        StartCoroutine(lento(2f));
    }
    //Corutina que reactiva las acciones del boss
    IEnumerator lento(float time)
    {

        yield return new WaitForSeconds(time);
        attacking = true;

    }
    //Corutina que destruye la cuerda y devuelve las acciones al boss
    IEnumerator EndRope(GameObject corda, float time)
    {

        yield return new WaitForSeconds(time);
        Destroy(corda);
        yield return new WaitForSeconds(0.5f);
        attacking = true;
        yo.GetComponent<AILerp>().canMove = true;

    }
    //Corutina que reactiva las acciones del boss
    IEnumerator Recargar()
    {

        yield return new WaitForSeconds(cd);
        attacking = true;

    }

}
