﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matematicas : MonoBehaviour
{
    //GameObject de la siguiente forma en la que se dividira
    public GameObject nextForm;
    //Float del tamano que tendra al dividirse
    public float nextSize;
    //Divisiones que le quedan
    public int divisions = 2;
    //Vida maxima del boss
    public int maxLife;

    // Start is called before the first frame update
    void Start()
    {
        //Le damos velocidad de forma aleatoria
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(2f,4f), Random.Range(2f, 4f));
        if(divisions == 1)
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(this.gameObject.GetComponent<Rigidbody2D>().velocity.x + 0.5f, this.gameObject.GetComponent<Rigidbody2D>().velocity.y + 0.5f);
        }
        if (divisions == 0)
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(this.gameObject.GetComponent<Rigidbody2D>().velocity.x + 1f, this.gameObject.GetComponent<Rigidbody2D>().velocity.y + 1f);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(this.gameObject.GetComponent<enemy>().currHp < maxLife/2)
        {
            //Si el boss llega a la mitad de la vida y aun le quedan divisiones, en lugar de seguir hasta que muera, se divide en dos
            if (divisions >= 1)
            {
                
                GameObject a = Instantiate(nextForm, this.transform.parent);
                GameObject b = Instantiate(nextForm, this.transform.parent);
                //Los bosses tendran el tamano establecido y la mitad de la vida maxima del boss actual
                a.GetComponent<Matematicas>().maxLife = maxLife / 2;
                a.GetComponent<enemy>().currHp = maxLife / 2;
                a.GetComponent<enemy>().maxHp = maxLife / 2;
                a.transform.localScale = new Vector3(nextSize, nextSize, 1);
                a.GetComponent<Matematicas>().nextSize = nextSize / 2;
                a.GetComponent<Matematicas>().divisions--;
                a.transform.position = this.transform.position;

                b.GetComponent<Matematicas>().maxLife = maxLife / 2;
                b.GetComponent<enemy>().currHp = maxLife / 2;
                b.GetComponent<enemy>().maxHp = maxLife / 2;
                b.transform.localScale = new Vector3(nextSize, nextSize, 1);
                b.GetComponent<Matematicas>().nextSize = nextSize / 2;
                b.GetComponent<Matematicas>().divisions--;
                b.transform.position = this.transform.position;
                //Despues de crear los bosses se destruye el original
                Destroy(this.gameObject);

            }

        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si colisiona con un jugador o sustituto lo danara
        if (collision.gameObject.tag == "Player") collision.gameObject.GetComponent<playerController>().dolor(1);
        if (collision.gameObject.tag == "Substitute") collision.gameObject.GetComponent<AlmohadaController>().dolor(1);
    }

}
